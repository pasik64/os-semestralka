#pragma once

#define MAX_PATH 260

enum File_Type {
	File,
	Pipe,
	Stdstream,
	Procfs
};

enum Open_Mode : std::uint8_t {
	Write_Only,
	Read_Only,
	Read_Write
};

struct File_Descriptor {
	char path[MAX_PATH];
	uint64_t position_pointer;
	bool is_open;
	bool used;
	bool valid = true;
	Open_Mode acl;
	
	File_Type type;
};
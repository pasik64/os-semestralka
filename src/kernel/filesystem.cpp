#pragma once

#include "filesystem.h"

namespace filesystem {	

	std::map <char, uint8_t> disks; // pismeno disku, cislo disku

	uint8_t current_disk_number;
	char current_disk_char;
	kiv_fs_fat::fat current_fat_table;
	boot_record current_br;	

	std::string valid_filename(std::string filename) {
		size_t len = filename.length();
		
		if (len < 1) return "";			
		if (filename.front() == '\0') return "";
		
		if (len > 11) return "";

		size_t dot_position = filename.rfind('.');
		if (dot_position != filename.npos) {
			if (dot_position > 8) {
				return "";
			}		
		}		

		return filename;
	}		

	int get_root_dir_offset() {
		return (current_br.fat_table_count * current_br.sectors_per_fat + current_br.reserved_sectors_count);
	}

	std::vector<char> get_v() {
		int size = current_fat_table.cluster_size_bytes;
		std::vector<char> vec = std::vector<char>(size, 0);
		return vec;
	}	

	// jedine odtud je volana operace s diskem
	void disk_sector_operation(char *data, uint64_t sector_index, uint64_t sector_count, kiv_hal::NDisk_IO param) {
		kiv_hal::TDisk_Address_Packet dap;
		dap.count = sector_count;
		dap.sectors = data;
		dap.lba_index = sector_index;

		kiv_hal::TRegisters regs;
		regs.rax.h = static_cast<uint8_t>(param);
		regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(&dap);
		regs.rdx.l = current_disk_number;
		kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);		
	}	

	bool Is_Directory(std::string filename, std::vector<uint16_t> indexes) {
		if (filename[0] == current_disk_char && filename[1] == ':') return true;
		
		int root_dir_offset = get_root_dir_offset();

		int i = 0;
		int index = -1;		
		uint16_t cluster_index = 0;
		uint64_t sector_index = 0;
		std::vector<char> v = get_v();
		char *data = v.data();

		// hledani slozky v nadrazene
		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;
			
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);						

			int index_in_dir = kiv_fs_fat::Get_Index_Inside_Dir(filename, data);
			if (index_in_dir != -1) {
				kiv_fs_fat::dir_entry entry;
				memcpy(&entry, data + index_in_dir, sizeof(kiv_fs_fat::dir_entry));
				kiv_os::NFile_Attributes attribute = entry.attributes;
				return (attribute == kiv_os::NFile_Attributes::Directory);
			}			

			v = get_v();
			data = v.data();
		}

		return false;
	}

	void delete_cluster_data(uint16_t cluster_index) {
		std::vector<char> v = get_v();
		char *arr = v.data();
		int root_dir_offset = get_root_dir_offset();
		disk_sector_operation(arr, cluster_index + root_dir_offset, 1, kiv_hal::NDisk_IO::Write_Sectors);
	}

	void save_fat_table() {	
		int cluster_size_bytes = current_fat_table.cluster_size_bytes;
		cluster_size_bytes = cluster_size_bytes * current_br.sectors_per_fat;
		char *arr = new char[cluster_size_bytes]();
		memset(arr, 0, cluster_size_bytes);

		int used = current_fat_table.cluster_count;
		int offset = current_br.reserved_sectors_count;
		int i = 0;
		for (i = 0; i < current_br.fat_table_count; i++) {
			memcpy(arr, (char *)current_fat_table.table, used);
			disk_sector_operation(arr, offset + (current_br.sectors_per_fat * i), current_br.sectors_per_fat, kiv_hal::NDisk_IO::Write_Sectors);
		}
	}

	void write_new_dir_to_cluster(uint16_t parent_index, uint16_t cluster_index) {		
		// .
		kiv_fs_fat::dir_entry current_dir;
		strncpy_s(current_dir.filename, ".", 1);
		current_dir.attributes = kiv_os::NFile_Attributes::Directory;
		current_dir.start_cluster = cluster_index;
		current_dir.filesize = 0;

		// ..
		kiv_fs_fat::dir_entry parent_dir;
		strncpy_s(parent_dir.filename, "..", 2);
		parent_dir.attributes = kiv_os::NFile_Attributes::Directory;
		parent_dir.start_cluster = parent_index;
		parent_dir.filesize = 0;

		char parent[sizeof(kiv_fs_fat::dir_entry)] = { 0 };
		memcpy(parent, &parent_dir, sizeof(kiv_fs_fat::dir_entry));

		std::vector<char> v = get_v();
		char *arr = v.data();
		memcpy(arr, &current_dir, sizeof(kiv_fs_fat::dir_entry));

		int i = 0;
		int index = sizeof(kiv_fs_fat::dir_entry);
		for (i = 0; i < sizeof(kiv_fs_fat::dir_entry); i++) {
			arr[index + i] = parent[i];
		}

		int sector_index = get_root_dir_offset() + cluster_index;
		disk_sector_operation(arr, sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);	
	}

	void init_root_directory() {
		kiv_fs_fat::dir_entry root;		
		strncpy_s(root.filename, &current_disk_char, 1);
		root.attributes = kiv_os::NFile_Attributes::Directory;
		root.start_cluster = 0;
		root.filesize = 0;
		
		std::vector<char> vec = get_v();
		char *arr = vec.data();
		memcpy(arr, &root, sizeof(root));
		disk_sector_operation(arr, get_root_dir_offset(), 1, kiv_hal::NDisk_IO::Write_Sectors);
	}

	void create_filesystem(kiv_hal::TDrive_Parameters params) {	
		// init boot record	
		uint64_t boot_sector_index = 0;
		boot_record br;
		br.bytes_per_sector = params.bytes_per_sector;
		br.sectors_per_cluster = SECTORS_PER_CLUSTER;
		br.reserved_sectors_count = RESERVED_SECTORS_COUNT;
		br.fat_table_count = FAT_TABLE_COUNT;
		br.root_dir_entries = br.bytes_per_sector / sizeof(kiv_fs_fat::dir_entry);
		br.total_sectors_count = static_cast<int16_t>(params.absolute_number_of_sectors);
		br.sectors_per_fat = SECTORS_PER_FAT;
		current_br = br;

		// init fat table 
		int cluster_count = (current_br.total_sectors_count - 1 - (current_br.fat_table_count *  current_br.sectors_per_fat) - 1);
		int cluster_size_bytes = (current_br.bytes_per_sector * current_br.sectors_per_cluster);
		current_fat_table = *(kiv_fs_fat::Init_FAT_Table(cluster_count, cluster_size_bytes));

		
		// ulozeni fat table
		save_fat_table();

	}
	
	uint16_t get_cluster_index(std::string name, uint16_t parent_index) {
		if (name[0] == current_disk_char) return parent_index;
		
		std::vector<char> v = get_v();
		char *data = v.data();
		int root_dir_offset = get_root_dir_offset();
		disk_sector_operation(data, parent_index + root_dir_offset, 1, kiv_hal::NDisk_IO::Read_Sectors);

		int index_in_dir = kiv_fs_fat::Get_Index_Inside_Dir(name, data);
		if (index_in_dir != -1) {
			kiv_fs_fat::dir_entry entry;
			memcpy(&entry, data + index_in_dir, sizeof(kiv_fs_fat::dir_entry));
			return entry.start_cluster;			
		}		

		return -1;
	}

	uint16_t get_cluster_index(std::string path) {
		if (path[0] != current_disk_char) return -1;

		std::string delimiter = "\\";

		size_t pos = 0;
		std::string token = "";
		int i = 0;
		uint16_t index = 0;
		while ((pos = path.find(delimiter)) != std::string::npos) {
			token = path.substr(0, pos);			
			index = get_cluster_index(token, index);			
			path.erase(0, pos + delimiter.length());
		}

		if (!path.empty()) {
			index = get_cluster_index(path, index);
		}
		return index;
	}
	
	void enlarge_filesize(std::vector<uint16_t> parent_indexes, char *filename, std::vector<uint16_t> indexes, uint32_t filesize, uint32_t new_file_size) {		
		int i = 0;
		int root_dir_offset = get_root_dir_offset();

		int file_index = -1;
		uint16_t file_cluster_index = 0;		
		uint16_t parent_cluster_index = 0;
		uint64_t parent_sector_index = 0;
		std::vector<char> parent_v = get_v();
		char *parent_data = parent_v.data();

		for (i = 0; i < parent_indexes.size(); i++) {
			parent_cluster_index = parent_indexes.at(i);
			parent_sector_index = root_dir_offset + parent_cluster_index;
			
			parent_v = get_v();
			parent_data = parent_v.data();
			disk_sector_operation(parent_data, parent_sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);
			kiv_fs_fat::dir_entry parent;
			memcpy(&parent, parent_data, sizeof(kiv_fs_fat::dir_entry));
			
			file_index = kiv_fs_fat::Get_Index_Inside_Dir(filename, parent_data);
			file_cluster_index = get_cluster_index(filename, parent_cluster_index);

			if (file_index != -1) break;
		}

		kiv_fs_fat::dir_entry entry;
		strncpy_s(entry.filename, filename, kiv_fs_fat::FILENAME_LEN);
		entry.attributes = (kiv_os::NFile_Attributes) 0x00;
		entry.start_cluster = file_cluster_index;

		uint32_t append_size = new_file_size - filesize;
		uint32_t skip_cluster_count = filesize / current_fat_table.cluster_size_bytes;
		indexes.erase(indexes.begin(), indexes.begin() + skip_cluster_count);
		filesize -= skip_cluster_count * current_fat_table.cluster_size_bytes;

		size_t needed_added_cluster_count = append_size / current_fat_table.cluster_size_bytes;
		int modulo = append_size % current_fat_table.cluster_size_bytes;
		if (modulo != 0) needed_added_cluster_count++;	

		uint16_t first_free = indexes.front();
		indexes.erase(indexes.begin(), indexes.begin() + 1);

		for (i = 0; i < needed_added_cluster_count; i++) {
			uint16_t free = 0;
			kiv_fs_fat::Set_Cluster_To_EOF(first_free);
			if (i != needed_added_cluster_count - 1) {
				if (!indexes.empty()) {
					free = indexes.front();
					indexes.erase(indexes.begin(), indexes.begin() + 1);
				}
				else {
					free = kiv_fs_fat::Find_Free_Cluster();
				}

				kiv_fs_fat::Fat_Concat_Cluster(first_free, free);
			}

			first_free = free;			
		}

		entry.filesize = new_file_size;

		// zapis do nadrazene slozky
		char arr[sizeof(kiv_fs_fat::dir_entry)] = { 0 };
		memcpy(arr, &entry, sizeof(kiv_fs_fat::dir_entry));
		for (i = 0; i < sizeof(kiv_fs_fat::dir_entry); i++) {
			parent_data[file_index + i] = arr[i];
		}
		 
		disk_sector_operation(parent_data, parent_sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);
		save_fat_table();	
	}
	
	void reduce_filesize(std::vector<uint16_t> parent_indexes, char *filename, std::vector<uint16_t> indexes, uint32_t filesize, uint32_t new_file_size) {
		int i = 0;
		int root_dir_offset = get_root_dir_offset();

		int file_index = -1;
		uint16_t file_cluster_index = 0;	
		uint16_t parent_cluster_index = 0;
		uint64_t parent_sector_index = 0;
		std::vector<char> parent_v = get_v();
		char *parent_data = parent_v.data();

		for (i = 0; i < parent_indexes.size(); i++) {
			parent_cluster_index = parent_indexes.at(i);
			parent_sector_index = root_dir_offset + parent_cluster_index;
			
			parent_v = get_v();
			parent_data = parent_v.data();
			disk_sector_operation(parent_data, parent_sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);
			kiv_fs_fat::dir_entry parent;
			memcpy(&parent, parent_data, sizeof(kiv_fs_fat::dir_entry));

			file_index = kiv_fs_fat::Get_Index_Inside_Dir(filename, parent_data);
			file_cluster_index = get_cluster_index(filename, parent_cluster_index);

			if (file_index != -1) break;
		}

		kiv_fs_fat::dir_entry entry;
		strncpy_s(entry.filename, filename, kiv_fs_fat::FILENAME_LEN);
		entry.attributes = (kiv_os::NFile_Attributes) 0x00;
		entry.start_cluster = file_cluster_index;

		uint32_t new_all_cluster_count = new_file_size / current_fat_table.cluster_size_bytes;
		int modulo = new_file_size % current_fat_table.cluster_size_bytes;
		if (modulo != 0) new_all_cluster_count++;

		// vymazani clusteru navic
		for (i = new_all_cluster_count; i < indexes.size(); i++) {
			uint16_t curr = indexes.at(i);
			kiv_fs_fat::Delete_Cluster_In_FAT(curr);
			delete_cluster_data(curr);
		}

		uint16_t cluster_index = indexes.at(new_all_cluster_count - 1);
		uint64_t sector_index = root_dir_offset + cluster_index;
		uint32_t position_in_cluster = new_file_size - ((new_all_cluster_count-1) * current_fat_table.cluster_size_bytes);

		std::vector<char> v = get_v();
		char *data = v.data();
		disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);

		std::vector<char> empty_v = get_v();
		char *empty_data = empty_v.data();
		memcpy(empty_data, data, position_in_cluster);
		disk_sector_operation(empty_data, sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);		
		entry.filesize = new_file_size;

		// zapis do nadrazene slozky
		char arr[sizeof(kiv_fs_fat::dir_entry)] = { 0 };
		memcpy(arr, &entry, sizeof(kiv_fs_fat::dir_entry));
		for (i = 0; i < sizeof(kiv_fs_fat::dir_entry); i++) {
			parent_data[file_index + i] = arr[i];
		}
	
		disk_sector_operation(parent_data, parent_sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);
		save_fat_table(); 
	}

	void add_file(char *filename, std::vector<uint16_t> indexes) {
		int i = 0;
		int root_dir_offset = get_root_dir_offset();

		int index = -1;		
		uint16_t cluster_index = 0;
		uint64_t sector_index = 0;
		std::vector<char> v = get_v();
		char *data = v.data();

		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;
		
			v = get_v();
			data = v.data();
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);
			kiv_fs_fat::dir_entry parent;
			memcpy(&parent, data, sizeof(kiv_fs_fat::dir_entry));
			
			index = kiv_fs_fat::Get_Free_Index_Inside_Dir(data);
			if (index != -1) break;
		}

		kiv_fs_fat::dir_entry entry;
		strncpy_s(entry.filename, filename, sizeof(entry.filename));
		entry.attributes = (kiv_os::NFile_Attributes) 0x00;
		uint16_t first_free = kiv_fs_fat::Find_Free_Cluster();
		entry.start_cluster = first_free;
		entry.filesize = 0;			
		kiv_fs_fat::Set_Cluster_To_EOF(first_free);
		
		if (index < 0) { // zadny index nenalezen, alokujeme novy cluster			
			int free_cluster = kiv_fs_fat::Find_Free_Cluster();

			char *arr = get_v().data();
			memcpy(arr, &entry, sizeof(kiv_fs_fat::dir_entry));
			disk_sector_operation(arr, free_cluster + get_root_dir_offset(), 1, kiv_hal::NDisk_IO::Write_Sectors);

			kiv_fs_fat::Fat_Concat_Cluster(indexes.back(), free_cluster);
			kiv_fs_fat::Set_Cluster_To_EOF(free_cluster);
		}
		else {
			// zapis do nadrazene slozky
			char arr[sizeof(kiv_fs_fat::dir_entry)] = { 0 };
			memcpy(arr, &entry, sizeof(kiv_fs_fat::dir_entry));
			for (i = 0; i < sizeof(kiv_fs_fat::dir_entry); i++) {
				data[index + i] = arr[i];
			}
			
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);
		}

		save_fat_table();
	}

	void Write_To_File(char *filename, uint32_t filesize, std::vector<uint16_t> parent_indexes, size_t buffer_size, char *buffer, size_t *actual_written_bytes, uint64_t current_position) {
		uint64_t i = 0;
		int root_dir_offset = get_root_dir_offset();		
		uint64_t original_position = current_position;

		int file_index = -1;
		uint16_t file_cluster_index = 0;		
		uint16_t parent_cluster_index = 0;
		uint64_t parent_sector_index = 0;
		std::vector<char> parent_v = get_v();
		char *parent_data = parent_v.data();

		for (i = 0; i < parent_indexes.size(); i++) {
			parent_cluster_index = parent_indexes.at(i);
			parent_sector_index = root_dir_offset + parent_cluster_index;
		
			parent_v = get_v();
			parent_data = parent_v.data();
			disk_sector_operation(parent_data, parent_sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);
			kiv_fs_fat::dir_entry parent;
			memcpy(&parent, parent_data, sizeof(kiv_fs_fat::dir_entry));

			file_index = kiv_fs_fat::Get_Index_Inside_Dir(filename, parent_data);
			file_cluster_index = get_cluster_index(filename, parent_cluster_index);			

			if (file_index != -1) break;
		}				

		kiv_fs_fat::dir_entry entry;
		strncpy_s(entry.filename, filename, kiv_fs_fat::FILENAME_LEN);
		entry.attributes = (kiv_os::NFile_Attributes) 0x00;		
		entry.start_cluster = file_cluster_index;		

		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(file_cluster_index);

		// aktualni cluster index, od ktereho budeme zapisovat
		uint64_t current_cluster_index = original_position / current_fat_table.cluster_size_bytes;
		current_position -= current_cluster_index * current_fat_table.cluster_size_bytes;

		size_t sum_all_cluster_count = (original_position + buffer_size) / current_fat_table.cluster_size_bytes;
		int modulo = (original_position + buffer_size) % current_fat_table.cluster_size_bytes;
		if (modulo != 0) sum_all_cluster_count++;

		*actual_written_bytes = 0;

		uint16_t cluster_index = 0;
		for (i = current_cluster_index; i < sum_all_cluster_count; i++) {

			if (current_cluster_index >= indexes.size()) { // musime alokovat novy
				uint16_t new_cluster_index = kiv_fs_fat::Find_Free_Cluster();
				kiv_fs_fat::Fat_Concat_Cluster(indexes.back(), new_cluster_index);
				kiv_fs_fat::Set_Cluster_To_EOF(new_cluster_index);
				cluster_index = new_cluster_index;
			}
			else {
				cluster_index = indexes.at(current_cluster_index);
			}
			int sector_index = get_root_dir_offset() + cluster_index;

			std::vector<char> v = get_v();
			char *data = v.data();
			size_t len = buffer_size;
			if (len > (v.size() - current_position)) {
				len = v.size();
			}
						
			if (current_position != 0) { // musime nejdrive precist				
				disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);

				if (buffer_size > (v.size() - current_position)) {
					buffer_size = v.size();
				}				
			}

			memcpy(data + current_position, buffer, len);
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);						

			*actual_written_bytes += len;			
			buffer += len;
			buffer_size -= len;
			current_position += len;
		}		

		entry.filesize = (uint32_t) (original_position + *actual_written_bytes);

		// zapis do nadrazene slozky
		char arr[sizeof(kiv_fs_fat::dir_entry)] = { 0 };
		memcpy(arr, &entry, sizeof(kiv_fs_fat::dir_entry));
		for (i = 0; i < sizeof(kiv_fs_fat::dir_entry); i++) {
			parent_data[file_index + i] = arr[i];
		}
		
		disk_sector_operation(parent_data, parent_sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);
		save_fat_table();
	}

	void add_dir(char *filename, std::vector<uint16_t> indexes) {
		int i = 0;
		int root_dir_offset = get_root_dir_offset();		

		int index = -1;		
		uint16_t cluster_index = 0;
		uint64_t sector_index = 0;
		std::vector<char> v = get_v();
		char *data = v.data();

		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;
		
			v = get_v();
			data = v.data();
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);
			kiv_fs_fat::dir_entry parent;
			memcpy(&parent, data, sizeof(kiv_fs_fat::dir_entry));

			index = kiv_fs_fat::Get_Free_Index_Inside_Dir(data);
			if (index != -1) break;
		}

		kiv_fs_fat::dir_entry entry;
		strncpy_s(entry.filename, filename, kiv_fs_fat::FILENAME_LEN);
		entry.attributes = kiv_os::NFile_Attributes::Directory;
		int free = kiv_fs_fat::Find_Free_Cluster();
		entry.start_cluster = free;
		entry.filesize = 0;
		kiv_fs_fat::Set_Cluster_To_EOF(free);
		write_new_dir_to_cluster(cluster_index, free);
		save_fat_table();

		if (index < 0) { 			
			int free_cluster = kiv_fs_fat::Find_Free_Cluster();

			memcpy(get_v().data(), &entry, sizeof(kiv_fs_fat::dir_entry));
			disk_sector_operation(get_v().data(), free_cluster + get_root_dir_offset(), 1, kiv_hal::NDisk_IO::Write_Sectors);

			kiv_fs_fat::Fat_Concat_Cluster(indexes.back(), free_cluster);
			kiv_fs_fat::Set_Cluster_To_EOF(free_cluster);
		}
		else {			
			char arr[sizeof(kiv_fs_fat::dir_entry)] = { 0 };
			memcpy(arr, &entry, sizeof(kiv_fs_fat::dir_entry));
			for (i = 0; i < sizeof(kiv_fs_fat::dir_entry); i++) {
				data[index + i] = arr[i];
			}
		
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);
		}
	}	

	bool check_dir_empty(std::string filename, uint16_t parent_index) {
		std::vector<char> v;
		char *data;

		uint16_t cluster_index = get_cluster_index(filename, parent_index);		
		uint16_t sector_index = get_root_dir_offset() + cluster_index;

		v = get_v();
		data = v.data();
		disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);		

		int index = kiv_fs_fat::Get_Last_Occupied_Index_Inside_Dir(data);
		if (index < 0) return true;

		if (cluster_index == 0) { // root
			return (index == 0);
		}
		else {
			return (index == (1 * sizeof(kiv_fs_fat::dir_entry)));
		}
	}

	void get_dir_content(std::vector<uint16_t> indexes, uint32_t filesize, char *buffer, size_t buffer_size, size_t *actual_read_bytes, bool is_dir, uint64_t position) {
		bool is_root = (indexes.front() == 0);
		int root_dir_offset = get_root_dir_offset();				
		uint16_t cluster_index;
		uint64_t sector_index;
		std::vector<char> v;
		if (position != 0) {
			v = get_v();
		}
		else {
			v.resize(current_br.root_dir_entries * sizeof(kiv_fs_fat::dir_entry), 0);
		}
		char* data = v.data();
		*actual_read_bytes = 0;
		buffer[0] = 0;			

		int i = 0;

		uint64_t skip_cluster_count = position / current_fat_table.cluster_size_bytes;
		indexes.erase(indexes.begin(), indexes.begin() + skip_cluster_count);
		position -= skip_cluster_count * current_fat_table.cluster_size_bytes;
		
		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;
			
			v = get_v();
			data = v.data();
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);					

			if (is_dir) {
				char curr;
				int size = current_fat_table.cluster_size_bytes;
				int start_position_in_dir = (is_root) ? sizeof(kiv_fs_fat::dir_entry) : 0;
				for (i = start_position_in_dir; i < size; i = i + sizeof(kiv_fs_fat::dir_entry)) {
					curr = data[i];
					if (curr == 0) {						
						break;
					}
					else {
						kiv_fs_fat::dir_entry entry;
						memcpy(&entry, data + i, sizeof(kiv_fs_fat::dir_entry));

						kiv_os::TDir_Entry tdir_entry;						
						strncpy_s(tdir_entry.file_name, entry.filename, sizeof(entry.filename));
						tdir_entry.file_attributes = (uint16_t) entry.attributes;

						char ptr[sizeof(kiv_os::TDir_Entry)] = { 0 };						
						memcpy(ptr, &tdir_entry, sizeof(kiv_os::TDir_Entry));						
						memcpy(&buffer[*actual_read_bytes], &tdir_entry, sizeof(kiv_os::TDir_Entry));
						*actual_read_bytes += sizeof(struct kiv_os::TDir_Entry);
					}
				}				
				break;
			}
			else {
				data += position;
				position = 0;
				size_t len = strlen(data);
				if (len > (buffer_size - *actual_read_bytes)) {
					len = (buffer_size - *actual_read_bytes) - 1;
				}

				strncat_s(buffer, (buffer_size - *actual_read_bytes), data, len);
				size_t buff_len = strlen(buffer);
				*actual_read_bytes += buff_len;

				buffer += len;
			}								
		}		
	}

	kiv_os::NOS_Error get_size(std::string filename, std::vector<uint16_t> parent_indexes, uint32_t *size) {		
		int root_dir_offset = get_root_dir_offset();

		int i = 0;

		int index = -1;		
		uint16_t cluster_index;
		uint64_t sector_index;
		std::vector<char> v = get_v();
		char *data = v.data();
	
		for (i = 0; i < parent_indexes.size(); i++) {
			cluster_index = parent_indexes.at(i);
			sector_index = root_dir_offset + cluster_index;

			v = get_v();
			data = v.data();
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);
			
			index = kiv_fs_fat::Get_Index_Inside_Dir(filename, data);
			if (index != -1) break;
		}

		if (index == -1) {
			return kiv_os::NOS_Error::File_Not_Found;
		}
		
		kiv_fs_fat::dir_entry entry;
		memcpy(&entry, data+index, sizeof(kiv_fs_fat::dir_entry));
		*size = entry.filesize;
		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error delete_f(std::string filename, std::vector<uint16_t> indexes) {
		int root_dir_offset = get_root_dir_offset();

		int i = 0;

		int index = -1;		
		uint16_t cluster_index;
		uint64_t sector_index;
		std::vector<char> v = get_v();
		char *data = v.data();

		// nalezeni souboru v nadrazene slozce
		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;
		
			v = get_v();
			data = v.data();
			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);		

			index = kiv_fs_fat::Get_Index_Inside_Dir(filename, data);
			if (index != -1) break;
		}

		if (index == -1) {			
			return kiv_os::NOS_Error::File_Not_Found;
		}

		if (Is_Directory(filename, indexes) && !check_dir_empty(filename, cluster_index)) {
			return kiv_os::NOS_Error::Directory_Not_Empty;
		}

		// vynulovani entry
		char arr[sizeof(kiv_fs_fat::dir_entry)] = { 0 };
		for (i = 0; i < sizeof(kiv_fs_fat::dir_entry); i++) {
			data[index + i] = arr[i];
		}
		
		uint16_t cluster_i = get_cluster_index(filename, cluster_index);
		std::vector<uint16_t> all_indexes = kiv_fs_fat::Find_All_Indexes(cluster_i);

		for (i = 0; i < all_indexes.size(); i++) {
			uint16_t curr = all_indexes.at(i);
			kiv_fs_fat::Delete_Cluster_In_FAT(curr);
			delete_cluster_data(curr);
		}

		save_fat_table();
		disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Write_Sectors);
		return kiv_os::NOS_Error::Success;
	}

	std::pair<std::string, std::string> get_parent_path(const char *absolute_child_path) {
		std::string path(absolute_child_path);
		if (path.back() == '\\') {
			path.pop_back();
		}

		std::size_t found = path.find_last_of("\\");
		std::string parent_path = path.substr(0, found);
		std::string file = path.substr(found + 1);

		return (std::pair<std::string, std::string>(parent_path, file));
	}

	// vraci pair ("", "") pokud
	//		-> disk nebyl nalezeny
	//		-> jmeno souboru neni validni
	std::pair<std::string, std::string> load(const char *absolute_path) {
		std::pair<std::string, std::string> empty_pair("", "");

		if (disks.find(absolute_path[0]) != disks.end()) {
			current_disk_number = disks.at(absolute_path[0]);			

			std::pair<std::string, std::string> pair = get_parent_path(absolute_path);
			std::string parent_path = pair.first;
			if (!Check_File(parent_path.c_str())) return empty_pair;
			
			std::string filename = pair.second;

			std::string valid = valid_filename(filename);
			pair.second = valid;
			if (valid.length() < 1) return empty_pair;

			return pair;
		}

		return empty_pair;
	}

	bool Check_File(char *filename, std::vector<uint16_t> indexes) {
		int root_dir_offset = get_root_dir_offset();

		int i = 0;
		int index = -1;		
		uint16_t cluster_index = 0;
		uint64_t sector_index = 0;
		std::vector<char> v = get_v();
		char *data = v.data();
	
		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;

			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);
			kiv_fs_fat::dir_entry parent;
			memcpy(&parent, data, sizeof(kiv_fs_fat::dir_entry));
		
			index = kiv_fs_fat::Get_Index_Inside_Dir(filename, data);
			if (index != -1) break;

			v = get_v();
			data = v.data();
		}

		return (index != -1);
	}		

	void Initialize() {
		char init_disk_char = 'C';
		kiv_hal::TRegisters regs;
		for (regs.rdx.l = 0; ; regs.rdx.l++) {
			kiv_hal::TDrive_Parameters params;
			regs.rax.h = static_cast<uint8_t>(kiv_hal::NDisk_IO::Drive_Parameters);
			regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(&params);
			kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Disk_IO, regs);

			if (!regs.flags.carry) {
				regs.rax.l = static_cast<uint8_t>(kiv_os::NOS_File_System::Write_File);				

				uint8_t disk_number = regs.rdx.l;
				current_disk_number = disk_number;
				current_disk_char = init_disk_char++;

				disks[current_disk_char] = disk_number;				
				create_filesystem(params);						
			}

			if (regs.rdx.l == 255) break;
		}					
	}	

	bool Is_Directory(const char *absolute_path) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {
			return false;
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int index = get_cluster_index(parent_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(index);
		return Is_Directory(filename, indexes);
	}

	bool Check_File(const char *absolute_path) {
		if (absolute_path[0] == current_disk_char && absolute_path[1] == ':') {
			size_t len = strlen(absolute_path);
			int i;
			int count_other_chars = 0;
			for (i = 2; i < len; i++) {
				char c = absolute_path[i];
				if (c != '\\' && c != '/') {
					count_other_chars++;
				}				
			}

			if (count_other_chars == 0) return true;
		}

		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {			
			return false;
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int parent_index = get_cluster_index(parent_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(parent_index);

		return Check_File(&filename[0], indexes);
	}
	
	kiv_os::NOS_Error Get_File(const char *absolute_path, uint64_t position, char *buffer, size_t buffer_size, size_t *actual_read_bytes) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {			
			return kiv_os::NOS_Error::Invalid_Argument;
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;		
		
		int index = get_cluster_index(absolute_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(index);
		int parent_index = get_cluster_index(parent_path);
		std::vector<uint16_t> parent_indexes = kiv_fs_fat::Find_All_Indexes(parent_index);
		bool dir = Is_Directory(filename, parent_indexes);
		
		uint32_t filesize;
		get_size(filename, parent_indexes, &filesize);
		
		if (position < 0) return kiv_os::NOS_Error::Invalid_Argument;
		if (!dir) {
			if (position > filesize) {
				return kiv_os::NOS_Error::Invalid_Argument;
			}
			else if (position != filesize) {
				get_dir_content(indexes, filesize, buffer, buffer_size, actual_read_bytes, dir, position);
			}
		}
		else {
			get_dir_content(indexes, filesize, buffer, buffer_size, actual_read_bytes, dir, position);
		}
		
		return kiv_os::NOS_Error::Success;
	}
	
	kiv_os::NOS_Error Write_To_File(const char *absolute_path, uint64_t position, char *buffer, size_t buffer_size, size_t *actual_written_bytes) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {			
			return kiv_os::NOS_Error::Invalid_Argument;
		}		

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int parent_index = get_cluster_index(parent_path);
		std::vector<uint16_t> parent_indexes = kiv_fs_fat::Find_All_Indexes(parent_index);
		
		bool b = Check_File(&filename[0], parent_indexes);
		if (!b) {			
			return kiv_os::NOS_Error::Invalid_Argument;
		}		

		uint32_t filesize;
		get_size(filename, parent_indexes, &filesize);
		if (position < 0 || position > filesize) return kiv_os::NOS_Error::Invalid_Argument;		
		Write_To_File(&filename[0], filesize, parent_indexes, buffer_size, buffer, actual_written_bytes, position);

		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error Create_File(const char *absolute_path, bool directory) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {			
			return kiv_os::NOS_Error::Invalid_Argument;			
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int parent_index = get_cluster_index(parent_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(parent_index);

		bool b = Check_File(&filename[0], indexes);
		if (b) return kiv_os::NOS_Error::Invalid_Argument;

		if (directory) {
			add_dir(&filename[0], indexes);
		}
		else {
			add_file(&filename[0], indexes);
		}

		return kiv_os::NOS_Error::Success;
	}
		
	kiv_os::NOS_Error Remove_File(const char *absolute_path) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {			
			return kiv_os::NOS_Error::Invalid_Argument;
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int parent_index = get_cluster_index(parent_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(parent_index);
		return delete_f(filename, indexes);
	}  

	kiv_os::NOS_Error Get_Filesize(const char *absolute_path, uint32_t *size) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {
			return kiv_os::NOS_Error::Invalid_Argument;
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;				

		int parent_index = get_cluster_index(parent_path);
		std::vector<uint16_t> parent_indexes = kiv_fs_fat::Find_All_Indexes(parent_index);
		return get_size(filename, parent_indexes, size);
	}

	kiv_os::NOS_Error Set_Filesize(char *absolute_path, uint32_t new_filesize) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {
			return kiv_os::NOS_Error::Invalid_Argument;
		}		

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int index = get_cluster_index(absolute_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(index);
		int parent_index = get_cluster_index(parent_path);
		std::vector<uint16_t> parent_indexes = kiv_fs_fat::Find_All_Indexes(parent_index);
		
		bool b = Check_File(&filename[0], parent_indexes);
		if (!b) return kiv_os::NOS_Error::File_Not_Found;	
		
		bool dir = Is_Directory(filename, parent_indexes);
		if (dir) return kiv_os::NOS_Error::Invalid_Argument;

		uint32_t filesize;
		get_size(filename, parent_indexes, &filesize);

		if (filesize > new_filesize) {			
			reduce_filesize(parent_indexes, &filename[0], indexes, filesize, new_filesize);
		}
		else if (filesize < new_filesize) {			
			enlarge_filesize(parent_indexes, &filename[0], indexes, filesize, new_filesize);
		}
		
		return kiv_os::NOS_Error::Success;
	}

	void Get_Root(char *root_path, size_t max) {
		std::string path(1, current_disk_char);
		path += ":\\";
		strncpy_s(root_path, max, path.c_str(), path.length());
	}

	kiv_os::NFile_Attributes Get_Attribute(const char* absolute_path) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {
			return (kiv_os::NFile_Attributes) - 1;
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int index = get_cluster_index(parent_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(index);
		return Get_Attribute(filename, indexes);
	}

	kiv_os::NFile_Attributes Get_Attribute(std::string filename, std::vector<uint16_t> indexes) {
		int root_dir_offset = get_root_dir_offset();

		int i = 0;
		int index = -1;
		uint16_t cluster_index = 0;
		uint64_t sector_index = 0;
		std::vector<char> v = get_v();
		char* data = v.data();
		kiv_fs_fat::dir_entry entry;
		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;

			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);

			int index_in_dir = kiv_fs_fat::Get_Index_Inside_Dir(filename, data);

			memcpy(&entry, data + index_in_dir, sizeof(kiv_fs_fat::dir_entry));
		}
		return entry.attributes;

	}

	kiv_os::NFile_Attributes Set_Attribute(const char* absolute_path, kiv_os::NFile_Attributes attribute) {
		std::pair<std::string, std::string> pair = load(absolute_path);
		if (pair.first.empty()) {
			return (kiv_os::NFile_Attributes) - 1;
		}

		std::string parent_path = pair.first;
		std::string filename = pair.second;

		int index = get_cluster_index(parent_path);
		std::vector<uint16_t> indexes = kiv_fs_fat::Find_All_Indexes(index);
		return Set_Attribute(filename, indexes, attribute);
	}

	kiv_os::NFile_Attributes Set_Attribute(std::string filename, std::vector<uint16_t> indexes, kiv_os::NFile_Attributes attribute) {
		int root_dir_offset = get_root_dir_offset();

		int i = 0;
		int index = -1;
		uint16_t cluster_index = 0;
		uint64_t sector_index = 0;
		std::vector<char> v = get_v();
		char* data = v.data();

		for (i = 0; i < indexes.size(); i++) {
			cluster_index = indexes.at(i);
			sector_index = root_dir_offset + cluster_index;

			disk_sector_operation(data, sector_index, 1, kiv_hal::NDisk_IO::Read_Sectors);

			int index_in_dir = kiv_fs_fat::Get_Index_Inside_Dir(filename, data);
			kiv_fs_fat::dir_entry entry;
			memcpy(&entry, data + index_in_dir, sizeof(kiv_fs_fat::dir_entry));

			memcpy(data + index_in_dir, &attribute, sizeof(kiv_fs_fat::dir_entry));

			return attribute;
		}
		return attribute;
	}
} 
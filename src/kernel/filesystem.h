#pragma once

#include <map>

#include "../api/api.h"
#include "fs_fat.h"

namespace filesystem {

	const int8_t SECTORS_PER_CLUSTER = 1;
	const int16_t RESERVED_SECTORS_COUNT = 1;
	const int8_t FAT_TABLE_COUNT = 2;
	const int16_t SECTORS_PER_FAT = 9;	

	struct boot_record {
		uint16_t bytes_per_sector;
		uint8_t sectors_per_cluster;
		uint16_t reserved_sectors_count;
		uint8_t fat_table_count;
		uint16_t root_dir_entries;
		uint16_t total_sectors_count;
		uint8_t ignore;
		uint16_t sectors_per_fat;
		uint16_t sectors_per_track;
		uint16_t heads_count;
		char reserved[10];
		uint8_t boot_signature;
		uint32_t volume_id;
		char volume_label[11];					
	};	

	void Initialize();

	bool Is_Directory(const char *absolute_path);

	bool Check_File(const char *absolute_path);

	// file or dir
	kiv_os::NOS_Error Get_File(const char *path, uint64_t position, char *buffer, size_t buffer_size, size_t *actual_read_bytes);

	kiv_os::NOS_Error Write_To_File(const char *path, uint64_t position, char *buffer, size_t buffer_size, size_t *actual_written_bytes);

	kiv_os::NOS_Error Create_File(const char *absolute_path, bool directory);

	kiv_os::NOS_Error Remove_File(const char *absolute_path);

	void Get_Root(char *root_path, size_t max);
	
	kiv_os::NOS_Error Get_Filesize(const char *absolute_path, uint32_t *size);

	kiv_os::NOS_Error Set_Filesize(char *absolute_path, uint32_t new_filesize);

	kiv_os::NFile_Attributes Get_Attribute(const char* absolute_path);

	kiv_os::NFile_Attributes Get_Attribute(std::string filename, std::vector<uint16_t> indexes);

	kiv_os::NFile_Attributes Set_Attribute(const char* absolute_path, kiv_os::NFile_Attributes attribute);

	kiv_os::NFile_Attributes Set_Attribute(std::string filename, std::vector<uint16_t> indexes, kiv_os::NFile_Attributes attribute);
}

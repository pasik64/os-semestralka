#include "fs_fat.h"

namespace kiv_fs_fat {
	
	fat f;

	fat *Init_FAT_Table(int size, int cluster_size) {
		f.cluster_count = size;
		f.cluster_size_bytes = cluster_size;
		
		f.table = new uint16_t[size];
		memset(f.table, 0, size);
		f.table[0] = FAT_FILE_END;
		//current_fat_table[0] = 0xf0ff;
		//current_fat_table[1] = FAT_FILE_END;			
		return &f;
	}

	std::vector<uint16_t> Find_All_Indexes(uint16_t first_index) {
		std::vector<uint16_t> indexes;

		uint16_t value = first_index;
		do {
			indexes.push_back(value);
			value = f.table[value];
		} while (value != FAT_FILE_END); //(value > FAT_FREE_CLUSTER && value < FAT_FILE_END) {

		return indexes;
	}

	void Set_Cluster_To_EOF(uint16_t cluster_index) {
		f.table[cluster_index] = FAT_FILE_END;
	}

	void Delete_Cluster_In_FAT(uint16_t cluster_index) {
		f.table[cluster_index] = FAT_FREE_CLUSTER;
	}

	uint16_t Find_Free_Cluster() {
		uint16_t i;
		uint16_t max = f.cluster_count;
		for (i = 0; i < max; i++) {
			if (f.table[i] == FAT_FREE_CLUSTER) {
				return i;
			}
		}

		return -1;
	}

	uint16_t Get_Last_Occupied_Index(uint16_t index) {
		uint16_t value = f.table[index];

		if (value == FAT_FILE_END) {
			return index;
		}
		else {//if (value > FAT_FREE_CLUSTER && value < FAT_FILE_END) {
			return Get_Last_Occupied_Index(value);
		}
	}

	void Fat_Concat_Cluster(uint16_t parent_cluster_index, uint16_t added_cluster) {
		f.table[parent_cluster_index] = added_cluster;
	}

	int Get_Index_Inside_Dir(std::string filename, char *data) {
		int i, j;
		char curr;
		int size = f.cluster_size_bytes;
		for (i = 0; i < size; i = i + sizeof(dir_entry)) {
			curr = data[i];

			for (j = 0; j < filename.size(); j++) {
				if (data[i + j] != filename[j]) break;

				if (j == filename.size() - 1) {
					return i;
				}
			}
		}

		return -1;
	}

	int Get_Free_Index_Inside_Dir(char *data) {
		int i;
		char curr;	
		int size = f.cluster_size_bytes;
		for (i = 0; i < size; i = i + sizeof(dir_entry)) {
			curr = data[i];
			if (curr == 0) {				
				return i;
			}
		}

		return -1;
	}

	int Get_Last_Occupied_Index_Inside_Dir(char *data) {
		int i, index = -1;
		char curr;
		int size = f.cluster_size_bytes;
		for (i = 0; i < size; i = i + sizeof(dir_entry)) {
			curr = data[i];
			if (curr != 0) {
				index = i;
			}
		}

		return index;
	}

}
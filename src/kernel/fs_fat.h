#pragma once

#ifndef FS_FAT_H
#define FS_FAT_H

#include <vector> 
#include <string>

#include "..\api\api.h"

namespace kiv_fs_fat {
	
	const uint16_t FAT_FREE_CLUSTER = 0x0000;
	//const uint16_t FAT_BAD_CLUSTER = 0xFFF7;
	const uint16_t FAT_FILE_END = 0xFFFF;
	
	const int8_t FILENAME_LEN = 11; //8+1+3

	struct fat {
		int cluster_count;
		uint16_t *table;
		int cluster_size_bytes;
	};

	struct dir_entry { // 32B
		char filename[FILENAME_LEN] = { 0 };		
		kiv_os::NFile_Attributes attributes;
		char reserved[9] = { 0 };
		char time[2] = { 0 };
		char date[2] = { 0 };
		uint16_t start_cluster;
		uint32_t filesize;
	};

	void Set_Cluster_To_EOF(uint16_t cluster_index);
	
	void Delete_Cluster_In_FAT(uint16_t cluster_index);	

	uint16_t Find_Free_Cluster();

	int Get_Index_Inside_Dir(std::string filename, char *data);

	int Get_Free_Index_Inside_Dir(char *data);

	int Get_Last_Occupied_Index_Inside_Dir(char *data);

	uint16_t Get_Last_Occupied_Index(uint16_t index);

	fat *Init_FAT_Table(int size, int cluster_size_bytes);

	std::vector<uint16_t> Find_All_Indexes(uint16_t first_index);

	void Fat_Concat_Cluster(uint16_t parent_cluster_index, uint16_t added_cluster);
}
#endif
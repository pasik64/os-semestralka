#include "io.h"
#include "kernel.h"
#include "handles.h"
#include "pipe.h"
#include <string>
#include <iostream>

namespace kiv_os_io {
	//variables
	std::vector<File_Descriptor>  fd_table;
	std::mutex fd_table_lock;
	char working_dir_path[MAX_PATH];

	void Initialize_IO() {
		// STDIN
		File_Descriptor std_in_desc;
		std_in_desc.acl = Open_Mode::Read_Only;
		std_in_desc.type = File_Type::Stdstream;
		std_in_desc.is_open = true;
		std_in_desc.position_pointer = 0;
		strcpy_s(std_in_desc.path, STDIN_PATH);
		fd_table.push_back(std_in_desc);

		// STDOUT
		File_Descriptor	std_out_desc;
		std_out_desc.acl = Open_Mode::Write_Only;
		std_out_desc.type = File_Type::Stdstream;
		std_out_desc.is_open = true;
		std_out_desc.position_pointer = 0;
		strcpy_s(std_out_desc.path, STDOUT_PATH);
		fd_table.push_back(std_out_desc);

		// working path
		char root_path[MAX_PATH];
		filesystem::Get_Root(root_path, MAX_PATH);
		strncpy_s(working_dir_path, root_path, MAX_PATH);
	}

	size_t Read_Line_From_Console(char* buffer, const size_t buffer_size) {
		kiv_hal::TRegisters registers;

		size_t pos = 0;
		while (pos < buffer_size) {
			//read char
			registers.rax.h = static_cast<decltype(registers.rax.l)>(kiv_hal::NKeyboard::Read_Char);
			kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::Keyboard, registers);

			if (!registers.flags.non_zero) break;	//nic jsme neprecetli, 
													//pokud je rax.l EOT, pak byl zrejme vstup korektne ukoncen
													//jinak zrejme doslo k chybe zarizeni

			char ch = registers.rax.l;

			//osetrime zname kody
			switch (static_cast<kiv_hal::NControl_Codes>(ch)) {
			case kiv_hal::NControl_Codes::BS: {
				//mazeme znak z bufferu
				if (pos > 0) pos--;

				registers.rax.h = static_cast<decltype(registers.rax.l)>(kiv_hal::NVGA_BIOS::Write_Control_Char);
				registers.rdx.l = ch;
				kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, registers);
			}
											break;

			case kiv_hal::NControl_Codes::LF:  break;	//jenom pohltime, ale necteme
			case kiv_hal::NControl_Codes::NUL:			//chyba cteni?
			case kiv_hal::NControl_Codes::CR:  return pos;	//docetli jsme az po Enter


			default: buffer[pos] = ch;
				pos++;
				registers.rax.h = static_cast<decltype(registers.rax.l)>(kiv_hal::NVGA_BIOS::Write_String);
				registers.rdx.r = reinterpret_cast<decltype(registers.rdx.r)>(&ch);
				registers.rcx.r = 1;
				kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, registers);
				break;
			}
		}

		return pos;

	}

	File_Descriptor* get_descriptor(kiv_os::THandle handle) {

		return &fd_table[handle];
	}

	void Handle_IO(kiv_hal::TRegisters& regs) {

		kiv_os::NOS_Error return_code = kiv_os::NOS_Error::Success;

		switch (static_cast<kiv_os::NOS_File_System>(regs.rax.l)) {

		case kiv_os::NOS_File_System::Open_File:
			return_code = Open_File(regs);
			break;

		case kiv_os::NOS_File_System::Write_File:
			return_code = Write_File(regs);
			break;

		case kiv_os::NOS_File_System::Read_File:
			return_code = Read_File(regs);
			break;

		case kiv_os::NOS_File_System::Seek:
			return_code = Seek(regs);
			break;
		case kiv_os::NOS_File_System::Close_Handle:
			return_code = Close_Handle(regs);
			break;
		case kiv_os::NOS_File_System::Delete_File:
			return_code = Delete_File(regs);
			break;

		case kiv_os::NOS_File_System::Set_Working_Dir:
			return_code = Set_Working_Dir(regs);
			break;

		case kiv_os::NOS_File_System::Get_Working_Dir:
			return_code = Get_Working_Dir(regs);
			break;
		case kiv_os::NOS_File_System::Set_File_Attribute:
			return_code = Set_File_Attribute(regs);
			break;
		case kiv_os::NOS_File_System::Get_File_Attribute:
			return_code = Get_File_Attribute(regs);
			break;
		case kiv_os::NOS_File_System::Create_Pipe:
			return_code = Create_Pipe(regs);
			break;
		}
		regs.flags.carry = return_code == kiv_os::NOS_Error::Success ? 0 : 1;
		if (return_code != kiv_os::NOS_Error::Success) regs.rax.r = (uint64_t)return_code;
	}

	kiv_os::NOS_Error Create_Pipe(kiv_hal::TRegisters& regs)
	{
		kiv_os::THandle* pipe = (kiv_os::THandle*)regs.rdx.r;
		uint16_t pipe_index = (uint16_t)(kiv_os_pipe::register_new_pipe());

		fd_table_lock.lock();
		File_Descriptor in;
		in.acl = Open_Mode::Write_Only;
		in.position_pointer = 0;
		in.is_open = true;
		in.type = File_Type::Pipe;
		sprintf_s(in.path, "%s%hu", PIPE_PATH_BEGINNING, pipe_index);
		fd_table.push_back(in);
		pipe[0] = (uint16_t)(fd_table.size() - 1);

		File_Descriptor out;
		out.acl = Open_Mode::Read_Only;
		out.position_pointer = 0;
		out.is_open = true;
		out.type = File_Type::Pipe;
		sprintf_s(out.path, "%s%hu", PIPE_PATH_BEGINNING, pipe_index);
		fd_table.push_back(out);
		pipe[1] = (uint16_t)(fd_table.size() - 1);
		fd_table_lock.unlock();

		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error Read_File(kiv_hal::TRegisters& regs)
	{
		uint16_t handle = (uint16_t)regs.rdx.r;
		char* buffer = reinterpret_cast<char*>(regs.rdi.r);
		size_t buffer_size = regs.rcx.r;

		if (handle >= fd_table.size()) {
			return kiv_os::NOS_Error::File_Not_Found;
		}
		File_Descriptor* fd = get_descriptor(handle);
		if (fd->valid == false) return kiv_os::NOS_Error::Invalid_Argument;
		if (fd->is_open == false) return kiv_os::NOS_Error::IO_Error;
		if (fd->acl == Open_Mode::Write_Only) {
			regs.flags.carry = (regs.rax.r == 0 ? 1 : 0);
			return kiv_os::NOS_Error::Permission_Denied;
		}
		switch (fd->type) {
		case File_Type::Stdstream: {
			regs.rax.r = Read_Line_From_Console(buffer, buffer_size);
			fd->position_pointer += regs.rax.r;
			return kiv_os::NOS_Error::Success;
		}
		case File_Type::Procfs: {
			size_t actual_read_bytes = 0;
			kiv_os::NOS_Error return_code = kiv_os_process::Write_Procfs(buffer, buffer_size, &actual_read_bytes);
			fd->position_pointer += actual_read_bytes;
			regs.rax.r = actual_read_bytes;
			return return_code;
		} break;
		case File_Type::File: {
			size_t actual_read_bytes = 0;
			kiv_os::NOS_Error ret_code = filesystem::Get_File(fd->path, fd->position_pointer, buffer, buffer_size, &actual_read_bytes);
			regs.rax.r = actual_read_bytes;
			fd->position_pointer += actual_read_bytes;
			return ret_code;
		}
		case File_Type::Pipe: {
			size_t actual_written_bytes = 0;
			uint16_t index = kiv_os_pipe::path_to_pipe(fd_table[handle].path);
			kiv_os::NOS_Error return_code = kiv_os_pipe::read_from_pipe(index, buffer, buffer_size, &actual_written_bytes);
			buffer[actual_written_bytes] = '\0';
			regs.rax.r = actual_written_bytes;
			fd->position_pointer += actual_written_bytes;
			return return_code;
		}
		default: {
			return kiv_os::NOS_Error::Unknown_Error;
		}
		}
	}

	kiv_os::NOS_Error Write_File(kiv_hal::TRegisters& regs)
	{
		kiv_os::THandle handle = (uint16_t)regs.rdx.r;
		char* buffer = reinterpret_cast<char*>(regs.rdi.r);
		size_t buffer_size = regs.rcx.r;

		File_Descriptor* fd = get_descriptor(handle);
		if (fd->valid == false) return kiv_os::NOS_Error::Invalid_Argument;
		if (fd->is_open == false) return kiv_os::NOS_Error::IO_Error;
		if (fd->acl == Open_Mode::Read_Only) {
			regs.flags.carry = (regs.rax.r == 0 ? 1 : 0);
			return kiv_os::NOS_Error::Permission_Denied;
		}
		switch (fd->type) {
		case File_Type::Stdstream: {
			kiv_hal::TRegisters registers;
			registers.rax.h = static_cast<decltype(registers.rax.h)>(kiv_hal::NVGA_BIOS::Write_String);
			registers.rdx.r = regs.rdi.r;
			registers.rcx = regs.rcx;

			//preklad parametru dokoncen, zavolame sluzbu
			kiv_hal::Call_Interrupt_Handler(kiv_hal::NInterrupt::VGA_BIOS, registers);

			regs.flags.carry |= (registers.rax.r == 0 ? 1 : 0);	//jestli jsme nezapsali zadny znak, tak jiste doslo k nejake chybe
			regs.rax = registers.rcx;	//VGA BIOS nevraci pocet zapsanych znaku, tak predpokladame, ze zapsal vsechny
			return kiv_os::NOS_Error::Success;
		}
		case File_Type::Pipe: {
			size_t actual_written_bytes = 0;
			uint16_t index = kiv_os_pipe::path_to_pipe(fd_table[handle].path);
			kiv_os::NOS_Error return_code = kiv_os_pipe::write_to_pipe(index, buffer, buffer_size, &actual_written_bytes);
			regs.rax.r = actual_written_bytes;
			fd->position_pointer += actual_written_bytes;
			return return_code;
		}
		case File_Type::File: {
			size_t actual_written_bytes = 0;
			kiv_os::NOS_Error return_code = filesystem::Write_To_File(fd->path, fd->position_pointer, buffer, buffer_size, &actual_written_bytes);
			regs.rax.r = actual_written_bytes;
			fd->position_pointer += actual_written_bytes;
			return return_code;
		}
		case File_Type::Procfs: {
			return kiv_os::NOS_Error::Invalid_Argument;
		}
		default: {
			return kiv_os::NOS_Error::Unknown_Error;
		}
		}
	}

	File_Type Get_File_Type(const char* file_name) {
		if (strcmp(file_name, STDIN_PATH) == 0 || strcmp(file_name, STDOUT_PATH) == 0) {
			return File_Type::Stdstream;
		} else if (strcmp(file_name, kiv_os_process::PROC_ADDRESS) == 0) {
			return File_Type::Procfs;
		}
		else if (strncmp(file_name, PIPE_PATH_BEGINNING, strlen(PIPE_PATH_BEGINNING)) == 0) {
			return File_Type::Pipe;
		}
		else {
			return File_Type::File;
		}
	}

	bool Is_Path_Absolute(char* path) {
		return path[0] >= 'A' && path[0] <= 'Z' && path[1] == ':' && path[2] == '\\';
	}

	std::string Make_Absolute_Path(char* path) {
		std::string temp(path);
		if (Is_Path_Absolute(path)) return temp;

		std::string result;
		std::string wdir(working_dir_path);

		if (wdir.back() == '\\' || wdir.back() == '/') wdir.pop_back();

		if (temp[0] == '\\' || temp[0] == '/') { // root
			result.append(wdir, 0, 3);
			temp.erase(0, 1);
		}
		else {
			result.append(wdir);
		}

		while (!temp.empty()) {
			if (temp.compare(0, 2, "..") == 0) { // vymazeme posledni slozku
				size_t pos = result.rfind('\\');
				if (pos == result.npos) {
					pos = result.rfind('/');
				}

				if (pos != result.npos) {
					result.erase(result.begin() + pos, result.end());
				}

				temp.erase(0, 2);
			}
			else if (temp[0] == '.') { // nic
				temp.erase(0, 1);
			}
			else {  // pridame slozku
				size_t pos = temp.find('\\');
				if (pos == temp.npos) {
					pos = temp.find('/');
					if (pos == temp.npos) {
						pos = temp.length();
					}
				}

				result += "\\";
				result.append(temp.begin(), temp.begin() + pos);
				temp.erase(temp.begin(), temp.begin() + pos);
			}

			while (temp[0] == '\\' || temp[0] == '/') { // smazeme oddelovace
				temp.erase(0, 1);
			}
		}

		// kdyz je to root, na konec pridame zpetne lomitko
		if (result[0] >= 'A' && result[0] <= 'Z' && result[1] == ':' && result.size() == 2) result += "\\";

		return result;
	}

	kiv_os::NOS_Error Open_File(kiv_hal::TRegisters& regs) {
		char* file_name = reinterpret_cast<char*>(regs.rdx.r);
		bool always_open = (regs.rcx.r & (uint64_t)kiv_os::NOpen_File::fmOpen_Always) != 0;
		bool read_only = (regs.rdi.r & (uint64_t)kiv_os::NFile_Attributes::Read_Only) != 0;
		bool directory = (regs.rdi.r & (uint64_t)kiv_os::NFile_Attributes::Directory) != 0;

		kiv_os::NOS_Error return_code = kiv_os::NOS_Error::Success;
		if (strcmp(file_name, STDIN_PATH) == 0) {
			regs.rax.r = std_in;
			return return_code;
		}

		if (strcmp(file_name, STDOUT_PATH) == 0) {
			regs.rax.r = std_out;
			return return_code;
		}

		File_Type ft = Get_File_Type(file_name);
		Open_Mode acl;
		uint64_t position;

		switch (ft) {
		case (Procfs): {
			acl = Open_Mode::Read_Only;
			position = 0;
		} break;
		case (Pipe): {
			regs.rax.r = atoi(&file_name[strlen(PIPE_PATH_BEGINNING)]);
			return kiv_os::NOS_Error::Success;
		} break;


		case (File): {
			std::string p = Make_Absolute_Path(file_name);
			const char* absolute_path = p.c_str();

			if (always_open) { // slozka/soubor musi existovat
				if (!filesystem::Check_File(absolute_path)) {
					return kiv_os::NOS_Error::File_Not_Found;
				}
			}
			else {
				if (filesystem::Check_File(absolute_path)) {
					if (directory) { // slozka s timto nazvem existuje -> invalid argument
						return kiv_os::NOS_Error::Invalid_Argument;
					}
					else { // soubor existuje -> smazeme
						filesystem::Remove_File(absolute_path);
					}
				}

				return_code = filesystem::Create_File(absolute_path, directory); // vytvorime novou slozku/soubor			
			}

			uint32_t file_size = 0;
			filesystem::Get_Filesize(absolute_path, &file_size);
			//position = (always_open == true) ? file_size : 0;			
			position = 0;
			file_name = _strdup(absolute_path);
			acl = (read_only == true) ? Open_Mode::Read_Only : Open_Mode::Read_Write;
		} break;

		default: {
			return kiv_os::NOS_Error::Unknown_Error;
		} break;

		}

		if (return_code == kiv_os::NOS_Error::Success) {
			File_Descriptor fd;
			fd.acl = acl;
			fd.type = ft;
			strcpy_s(fd.path, file_name);
			fd.is_open = true;
			fd.used = true;
			fd.position_pointer = position;


			fd_table.push_back(fd);
			regs.rax.r = (fd_table.size() - 1);
		}

		return return_code;
	}

	kiv_os::NOS_Error Seek(kiv_hal::TRegisters& regs) {
		uint64_t new_value = regs.rdi.r;
		if (regs.rdx.r >= fd_table.size()) {
			return kiv_os::NOS_Error::File_Not_Found;
		}
		File_Descriptor* fd = &(fd_table[regs.rdx.r]);
		kiv_os::NFile_Seek seek_type = (kiv_os::NFile_Seek)regs.rcx.l;

		if (!(fd->used)) {
			return kiv_os::NOS_Error::Invalid_Argument;
		}
		else if (!(fd->is_open)) {
			return kiv_os::NOS_Error::IO_Error;
		}

		uint32_t filesize = 0;
		kiv_os::NOS_Error return_code = filesystem::Get_Filesize(fd->path, &filesize);
		if (return_code != kiv_os::NOS_Error::Success) {
			return return_code;
		}

		switch (seek_type) {
		case kiv_os::NFile_Seek::Beginning:
			if (fd->position_pointer > filesize) {
				return kiv_os::NOS_Error::Invalid_Argument;
			}
			else {
				fd->position_pointer = new_value;
			}
			break;
		case kiv_os::NFile_Seek::Current:
			if ((fd->position_pointer + new_value) > filesize) {
				return kiv_os::NOS_Error::Invalid_Argument;
			}
			else {
				fd->position_pointer += new_value;
			}
			break;
		case kiv_os::NFile_Seek::End:
			if (filesize - new_value < 0) {
				return kiv_os::NOS_Error::Invalid_Argument;
			}
			else {
				fd->position_pointer = filesize - new_value;
			}
			break;
		case kiv_os::NFile_Seek::Get_Position:
			regs.rax.r = fd->position_pointer;
			break;
		case kiv_os::NFile_Seek::Set_Position:
			if (fd->position_pointer > filesize) {
				return kiv_os::NOS_Error::Invalid_Argument;
			}
			else {
				fd->position_pointer = new_value;
			}
			break;
		case kiv_os::NFile_Seek::Set_Size:
			fd->position_pointer = new_value;
			return filesystem::Set_Filesize(fd->path, (uint32_t)fd->position_pointer);
			break;
		}
		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error Close_Handle(kiv_hal::TRegisters& regs) {
		uint16_t handle = (uint16_t)regs.rdx.r;
		fd_table_lock.lock();
		char* file_name = fd_table[handle].path;
		uint8_t open_mode = fd_table[handle].acl;
		fd_table_lock.unlock();
		uint16_t pipe_index = kiv_os_pipe::path_to_pipe(file_name);
		switch (Get_File_Type(file_name)) {
		case File_Type::Pipe:
			if (open_mode == Open_Mode::Read_Only) {
				std::cout << "";
				kiv_os_pipe::close_read(pipe_index);
				std::cout << "";
				//close pipe_out
			}
			if (open_mode == Open_Mode::Write_Only) {
				std::cout << "";
				kiv_os_pipe::close_write(pipe_index);
				std::cout << "";
				//close pipe_in
			}

			Make_Absolute_Path(file_name);
			return filesystem::Remove_File(file_name);
		case File_Type::Procfs:
			return kiv_os::NOS_Error::Success;
		case File_Type::File:
			return kiv_os::NOS_Error::Success;
		case File_Type::Stdstream:
			return kiv_os::NOS_Error::Success;
		default: return kiv_os::NOS_Error::IO_Error;
		}
	}

	kiv_os::NOS_Error Delete_File(kiv_hal::TRegisters& regs) {
		char* file = reinterpret_cast<char*>(regs.rdx.r);

		if (Get_File_Type(file) != File_Type::File) {
			return kiv_os::NOS_Error::Invalid_Argument;
		}
		else {
			return filesystem::Remove_File(Make_Absolute_Path(file).c_str());
		}
	}

	kiv_os::NOS_Error Set_Working_Dir(kiv_hal::TRegisters& regs) {
		char* path = reinterpret_cast<char*>(regs.rdx.r);
		std::string absolute_path = Make_Absolute_Path(path);

		if (!filesystem::Check_File(absolute_path.c_str())) {
			return kiv_os::NOS_Error::File_Not_Found;
		}
		else if (!filesystem::Is_Directory(absolute_path.c_str())) {
			return kiv_os::NOS_Error::Invalid_Argument;
		}
		else {
			strncpy_s(working_dir_path, absolute_path.c_str(), MAX_PATH);
			return kiv_os::NOS_Error::Success;
		}

	}

	kiv_os::NOS_Error Get_Working_Dir(kiv_hal::TRegisters& regs) {
		char* path = reinterpret_cast<char*>(regs.rdx.r);
		size_t allowed_path_lenght = regs.rcx.r;
		size_t accual_path_lenght = strlen(working_dir_path);

		if (accual_path_lenght > allowed_path_lenght) {
			return kiv_os::NOS_Error::Unknown_Error;
		}
		else {
			strncpy_s(path, allowed_path_lenght, working_dir_path, accual_path_lenght);
			regs.rax.r = accual_path_lenght;
			return kiv_os::NOS_Error::Success;
		}

	}

	kiv_os::NOS_Error Set_File_Attribute(kiv_hal::TRegisters& regs) {
		char* file = reinterpret_cast<char*>(regs.rdx.r);
		kiv_os::NFile_Attributes file_attributes = (kiv_os::NFile_Attributes)(regs.rdi.r);

		uint64_t result = (uint64_t)filesystem::Set_Attribute(Make_Absolute_Path(file).c_str(), file_attributes);

		if (result < 0) {
			return kiv_os::NOS_Error::File_Not_Found;
		}
		else if (result != (uint64_t)file_attributes) {
			return kiv_os::NOS_Error::Unknown_Error;
		}
		else {
			return kiv_os::NOS_Error::Success;
		}
	}

	kiv_os::NOS_Error Get_File_Attribute(kiv_hal::TRegisters& regs) {
		char* file = reinterpret_cast<char*>(regs.rdx.r);
		regs.rdi.r = (uint64_t)filesystem::Get_Attribute(Make_Absolute_Path(file).c_str());

		if (regs.rdi.r < 0) {
			return kiv_os::NOS_Error::File_Not_Found;
		}
		else {
			return kiv_os::NOS_Error::Success;
		}
	}
}

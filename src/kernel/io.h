#pragma once

#define MAX_FD_TABLE_SIZE 65535

#include "..\api\api.h"
#include <mutex>
#include "file_descriptor.h"
#include "filesystem.h"
namespace kiv_os_io {
	constexpr char* STDIN_PATH = "STDIN";
	constexpr char* STDOUT_PATH = "STDOUT";
	constexpr auto std_in = 0;
	constexpr auto std_out = 1;
	constexpr auto std_err = 2;
	static const size_t PIPE_SIZE = 65536;
	static const char* PIPE_PATH_BEGINNING = "pipe:";


	File_Type Get_File_Type(const char* file_name);
	bool Is_Path_Absolute(char* path);
	std::string Make_Absolute_Path(char* path);

	void Handle_IO(kiv_hal::TRegisters& regs);
	void Initialize_IO();
	kiv_os::NOS_Error Write_File(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Open_File(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Read_File(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Seek(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Close_Handle(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Delete_File(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Set_Working_Dir(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Get_Working_Dir(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Set_File_Attribute(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Get_File_Attribute(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Create_Pipe(kiv_hal::TRegisters& regs);
}
#pragma once

#include "kernel.h"
#include "io.h"
#include <Windows.h>
#include <iostream>

HMODULE User_Programs;

kiv_os::THandle PID = 0;

void Initialize_Kernel() {
	User_Programs = LoadLibraryW(L"user.dll");
}

void Shutdown_Kernel() {
	FreeLibrary(User_Programs);
}

void __stdcall Sys_Call(kiv_hal::TRegisters &regs) {

	switch (static_cast<kiv_os::NOS_Service_Major>(regs.rax.h)) {
	
		case kiv_os::NOS_Service_Major::File_System:		
			kiv_os_io::Handle_IO(regs);
			break;
		case kiv_os::NOS_Service_Major::Process:
			kiv_os_process::Handle_Process(regs);
			break;

	}

}

void Wait_For_Shell() {
	kiv_os::THandle handles[1]{ PID };
	kiv_hal::TRegisters regs;
	regs.rdx.r = (uint64_t)handles;
	regs.rcx.r = 1;
	kiv_os_process::Wait_For(regs);
	regs.rdx.r = regs.rax.r;
	kiv_os_process::Read_Exit_Code(regs);
}

void Initialize_Shell() {
	kiv_hal::TRegisters regs;

	char program[kiv_os_process::PROGRAM_NAME_SIZE];
	strcpy_s(program, "shell");
	regs.rcx.l = (uint8_t)kiv_os::NClone::Create_Process;
	regs.rdx.r = (uint64_t)program;
	regs.rdi.r = NULL;
	regs.rbx.e = ((kiv_os_io::std_in << 16) | kiv_os_io::std_out);
	regs.flags.carry = 1;
	kiv_os_process::Clone(regs);
	PID = (kiv_os::THandle)regs.rax.r;
}

void __stdcall Bootstrap_Loader(kiv_hal::TRegisters &context) {
	Initialize_Kernel();
	kiv_hal::Set_Interrupt_Handler(kiv_os::System_Int_Number, Sys_Call);

	filesystem::Initialize();
	kiv_os_io::Initialize_IO();
	Initialize_Shell();
	if (PID == (kiv_os::THandle)kiv_os::NOS_Error::Unknown_Error) {
		Shutdown_Kernel();
	}
	Wait_For_Shell();
	Shutdown_Kernel();
}


void Set_Error(const bool failed, kiv_hal::TRegisters &regs) {
	if (failed) {
		regs.flags.carry = true;
		regs.rax.r = GetLastError();
	}
	else
		regs.flags.carry = false;
}
#pragma once

#include "kernel.h"
#include "io.h"
#include "pipe.h"
#include <vector>

std::vector <kiv_os_pipe::Pipe *> pipes;
namespace kiv_os_pipe {
	/*
		konstruktor pro pipe
	*/
	Pipe::Pipe() {
		open_read = true;
		open_write = true;
		memset(pipe_buffer, 0, PIPE_SIZE);

		read_index = 0;
		write_index = 0;
		current_read_position = 0;
		current_write_position = 0;
	}

	bool Pipe::close_pipe_read() {
		buffer_lock.lock();
		open_read = false;
		buffer_lock.unlock();
		if (open_write) {
			cond_var.notify_all();
		} else {
			return true;
		}
		return false;
	}

	bool Pipe::close_pipe_write() {
		size_t written;
		while (write_index > 0 && open_read) {
			std::unique_lock<std::mutex> locker(mutex);
			cond_var.wait(locker);
		}
		char ending = 0;
		write(&ending, 1, &written);
		buffer_lock.lock();
		open_write = false;
		cond_var.notify_all();
		buffer_lock.unlock();

		if (!open_read & !open_write) {
			return true;
		}
		return false;
	}

	uint16_t path_to_pipe(char* path) {
		uint16_t pipe_index = atoi(&path[strlen(PIPE_PATH_BEGINNING)]);
		return pipe_index;
	}

	std::string pipe_to_path(uint16_t pipe_index) {
		std::string s = PIPE_PATH_BEGINNING + pipe_index;
		return s;
	}

	size_t register_new_pipe() {
		Pipe* p = new Pipe();
		pipes.push_back(p);
		return pipes.size() - 1;
	}

	kiv_os::NOS_Error Pipe::write(char* buffer, size_t buffer_size, size_t* actual_written_bytes) {
		if (open_write == false) return kiv_os::NOS_Error::Permission_Denied;
		std::unique_lock<std::mutex> locker(mutex);
		while (write_index == PIPE_SIZE && open_write) {
			cond_var.wait(locker);
		}
		bool overflow = false;
		size_t to_write;
		if (buffer_size > PIPE_SIZE - write_index) {
			overflow = true;
			to_write = PIPE_SIZE - write_index;
		}
		else {
			to_write = buffer_size;
		}

		buffer_lock.lock();
		memcpy_s(&pipe_buffer[write_index], to_write, buffer, to_write);
		write_index = write_index + to_write;
		current_write_position += to_write;
		buffer_lock.unlock();
		*actual_written_bytes = to_write;

		locker.unlock();
		cond_var.notify_all();
		if (overflow) {
			return write(&buffer[to_write], buffer_size - to_write, actual_written_bytes);
		}
		else {
			return kiv_os::NOS_Error::Success;
		}
	}

	kiv_os::NOS_Error Pipe::read(char *buffer, size_t size, size_t *actual_read) {
		if (open_read == false) {
			return kiv_os::NOS_Error::Permission_Denied;
		}

		std::unique_lock<std::mutex> locker(mutex);
		while (read_index == write_index && open_read) {
			cond_var.wait(locker);
		}
		size_t bytes_to_read = write_index - read_index < size ? write_index - read_index : size;

		buffer_lock.lock();
		memcpy_s(buffer, size, &pipe_buffer[read_index], bytes_to_read);
		write_index = write_index - bytes_to_read;
		read_index = 0;
		current_read_position += bytes_to_read;
		*actual_read = bytes_to_read;
		buffer_lock.unlock();

		locker.unlock();
		cond_var.notify_all();
		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error write_to_pipe(uint16_t pipe_index, char* buffer, size_t buffer_size, size_t* actual_written_bytes) {
		try {
			return pipes.at(pipe_index)->write(buffer, buffer_size, actual_written_bytes);
		}
		catch (const std::out_of_range) {
			return kiv_os::NOS_Error::File_Not_Found;
		}

		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error read_from_pipe(uint16_t pipe_index, char* buffer, size_t buffer_size, size_t* actual_written_bytes) {
		try {
			return pipes.at(pipe_index)->read(buffer, buffer_size, actual_written_bytes);
		}
		catch (const std::out_of_range) {
			return kiv_os::NOS_Error::File_Not_Found;
		}

		return kiv_os::NOS_Error::Success;
	}

	bool Pipe::is_destroyable() {
		return (!open_read && !open_write);
	}

	kiv_os::NOS_Error close_read(uint16_t pipe_index) {
		try {
			size_t index = (pipes.size() <= pipe_index ? pipes.size() - 1 : pipe_index);

			if (pipes.at(index)->close_pipe_read()) {
				pipes.at(index)->~Pipe();
				//pipes.erase(pipes.begin() + index);
			}
		}
		catch (const std::out_of_range) {
			return kiv_os::NOS_Error::File_Not_Found;
		}

		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error close_write(uint16_t pipe_index) {
		try {
			size_t index = (pipes.size() <= pipe_index ? pipes.size() - 1 : pipe_index);
			if (pipes.at(index)->close_pipe_write()) {
				pipes.at(index)->~Pipe();
				//pipes.erase(pipes.begin() + index);
			}
		}
		catch (const std::out_of_range) {
			return kiv_os::NOS_Error::File_Not_Found;
		}

		return kiv_os::NOS_Error::Success;
	}

	void delete_pipes() {
		for (auto &pipe : pipes) {
			delete(pipe);
		}
	}
}
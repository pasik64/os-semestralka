#pragma once

#include "kernel.h"
#include <mutex>
#include <condition_variable>

static const size_t PIPE_SIZE = 65536;
static const char *PIPE_PATH_BEGINNING = "pipe:";

namespace kiv_os_pipe {
	class Pipe {
	private:
		char pipe_buffer[PIPE_SIZE];
		
		bool open_read;
		bool open_write;

		size_t read_index; //soucasny index pro cteni z bufferu
		size_t write_index; //soucasny index pro zapis do bufferu
		size_t current_read_position; //kolik bylo precteno od vytvoreni pipe, neresime postupne mazani (tedy pozice v souboru)
		size_t current_write_position; //kolik bylo precteno od vytvoreni pipe, neresime postupne mazani (tedy pozice v souboru)
		std::mutex buffer_lock;
		std::mutex mutex;
		std::condition_variable cond_var;

	public:
		Pipe();
		~Pipe() {};
		bool Pipe::close_pipe_read();
		bool Pipe::close_pipe_write();
		bool Pipe::is_destroyable();
		kiv_os::NOS_Error Pipe::write(char *data, size_t size, size_t *actual_written);
		kiv_os::NOS_Error Pipe::read(char *data, size_t size, size_t *actual_read);
	};
	uint16_t path_to_pipe(char* path);
	std::string pipe_to_path(uint16_t pipe_index);
	size_t register_new_pipe();
	kiv_os::NOS_Error close_read(uint16_t pipe_index);
	kiv_os::NOS_Error close_write(uint16_t pipe_index);
	kiv_os::NOS_Error write_to_pipe(uint16_t pipe_index, char* buffer, size_t buffer_size, size_t* actual_written_bytes);
	kiv_os::NOS_Error read_from_pipe(uint16_t pipe_index, char* buffer, size_t buffer_size, size_t* actual_written_bytes);
}



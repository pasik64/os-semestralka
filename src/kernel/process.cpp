#pragma once

#include "process.h"
#include <iostream>
#include <string>

extern HMODULE User_Programs;

namespace kiv_os_process {

	//local variables
	std::mutex signal_m;
	std::mutex pcb_lock;
	std::mutex tcb_lock;
	std::mutex tid_map_lock;
	std::mutex kernel_lock;
	std::condition_variable kernel;
	std::map<std::thread::id, kiv_os::THandle> tid_to_thread;
	kiv_os::THandle signalized;
	process_manager pm;
	std::atomic<int16_t> threads = 0;

	void Handle_Process(kiv_hal::TRegisters& regs) {
		switch (static_cast<kiv_os::NOS_Process>(regs.rax.l)) {
		case kiv_os::NOS_Process::Clone:
			Clone(regs);
			break;
		case kiv_os::NOS_Process::Wait_For:
			Wait_For(regs);
			break;
		case kiv_os::NOS_Process::Read_Exit_Code:
			Read_Exit_Code(regs);
			break;
		case kiv_os::NOS_Process::Exit:
			Exit(regs);
			break;
		case kiv_os::NOS_Process::Shutdown:
			Shutdown(regs);
			break;
		case kiv_os::NOS_Process::Register_Signal_Handler:
			Register_Signal_Handler(regs);
			break;
		}
	}

	kiv_os::NOS_Error Clone(kiv_hal::TRegisters& regs) {
		switch (static_cast<kiv_os::NClone>(regs.rcx.l)) {
			case kiv_os::NClone::Create_Process:
				regs.rax.r = Create_Process(regs);
				return kiv_os::NOS_Error::Success;
			case kiv_os::NClone::Create_Thread:
				regs.rax.r = Create_Thread(regs.rdx.r, regs.rdi.r);
				return kiv_os::NOS_Error::Success;
			default:
				return kiv_os::NOS_Error::Unknown_Error;
		}

	}

	kiv_os::THandle Get_Thread_PID() {
		kiv_os::THandle PID = EMPTY_ENTRY;
		std::thread::id thread_id = std::this_thread::get_id();
		tid_map_lock.lock();
		if (tid_to_thread.find(thread_id) != tid_to_thread.end()) {
			kiv_os::THandle TID = tid_to_thread.at(thread_id);
			tcb_lock.lock();
			PID = pm.tcb[TID - MAX_PROCESS_COUNT].pid;
			tcb_lock.unlock();
		}
		tid_map_lock.unlock();
		return PID;
	}

	int Get_Thread_Index(kiv_os::THandle tid) {
		for (int i = 0; i < sizeof(pm.tcb); i++) {
			if (pm.tcb[i].id == tid) {
				return i;
			}
		}
		return -1;
	}

	int Get_Process_Index(int32_t pid) {
		for (int i = 0; i < sizeof(pm.pcb); i++) {
			if (pm.pcb[i].pid == pid) {
				return i;
			}
		}
		return -1;
	}

	int Get_Process_Index(kiv_os::THandle TID) {
		for (int i = 0; i < sizeof(pm.tcb); i++) {
			if (pm.tcb[i].id == TID) {
				return i;
			}
		}
		return -1;
	}

	void Entry_point(kiv_os::TThread_Proc program, kiv_hal::TRegisters regs, kiv_os::THandle TID)
	{
		std::thread::id thread_id = std::this_thread::get_id();
		int thread_index = Get_Thread_Index(TID);
		int process_index = Get_Process_Index(pm.tcb[thread_index].pid);

		if (pm.pcb[process_index].arguments) {
			regs.rdi.r = (uint64_t)pm.pcb[process_index].arguments;
		}

		tid_map_lock.lock();
		tid_to_thread.insert(std::pair<std::thread::id, kiv_os::THandle>(thread_id, TID));
		tid_map_lock.unlock();
		uint16_t result = ERROR_FLAG;

		result = (uint16_t)program(regs);
		tcb_lock.lock();
		End_thread(TID, result);
		tcb_lock.unlock();
		uint8_t index = TID - MAX_PROCESS_COUNT;
		if (threads <= 0) {
			kernel.notify_all();
		}
	}

	void End_thread(kiv_os::THandle TID, uint16_t result) {
		uint8_t index = TID - MAX_PROCESS_COUNT;
		signal_m.lock();
		signalized = TID - MAX_PROCESS_COUNT;
		signal_m.unlock();

		uint16_t notify_index = pm.tcb[index].to_notify;
		if (notify_index != EMPTY_ENTRY) {
			pcb_lock.lock();
			pm.pcb[notify_index].waiting_for_counter--;
			pm.pcb[notify_index].synch_var.notify_all();
			pcb_lock.unlock();
		}


		kiv_os::THandle proces_handle = pm.tcb[TID - MAX_PROCESS_COUNT].pid;
		Notify_Process(proces_handle, pm.tcb[TID - MAX_PROCESS_COUNT].return_value);
		pm.tcb[TID - MAX_PROCESS_COUNT].return_value = result;
		if (pm.tcb[TID - MAX_PROCESS_COUNT].thread.joinable()) {
			pm.tcb[TID - MAX_PROCESS_COUNT].thread.detach();
			signal_m.lock();
			threads--;
			signal_m.unlock();
		}
	}

	void Close_Handles(std::vector<file_entry> handles) {
		for (int i = 0; i < handles.size(); i++) {
			handles[i].open = false;
			kiv_hal::TRegisters regs;
			regs.rdx.r = handles[i].file_descriptor;
			kiv_os_io::Close_Handle(regs);
		}
	}

	void Notify_Process(kiv_os::THandle PID, uint16_t result) {
		pcb_lock.lock();
		pm.pcb[PID].thread_count--;

		if (pm.pcb[PID].thread_count == 0) {
			//end process
			pm.pcb[PID].state = kiv_os_process::Process_State::TERMINATED;
			pcb_lock.unlock();
			tcb_lock.unlock();
			Close_Handles(pm.pcb[PID].file_table);
			tcb_lock.lock();
			pcb_lock.lock();
			//pm.pcb[PID].file_table.clear();
			//pm.pcb[PID].threads.clear();
			kiv_os::THandle waiting_parent = pm.pcb[PID].parent_id;
			if (waiting_parent != EMPTY_ENTRY) {
				pm.pcb[waiting_parent].waiting_for_counter--;
				pm.pcb[waiting_parent].synch_var.notify_all();
			}
		}
		pm.pcb[PID].return_value = result;
		pcb_lock.unlock();
	}

	uint16_t process_manager::allocate_free_pid()
	{
		for (int i = 0; i < MAX_PROCESS_COUNT; i++) {
			if ((pm.pcb[i].pid == EMPTY_ENTRY && pm.pcb[i].state == kiv_os_process::Process_State::READY) || pm.pcb[i].state == kiv_os_process::Process_State::TERMINATED) {
				pm.pcb[i].pid = i;
				return i;
			}
		}
		return EMPTY_ENTRY;
	}

	int Create_Process(kiv_hal::TRegisters& regs) {
		uint16_t pid = pm.allocate_free_pid();
		kiv_os::THandle stdout_handle = (uint16_t)(regs.rbx.e & 0x0000ffff);
		kiv_os::THandle stdin_handle = (uint16_t)((regs.rbx.e & 0xffff0000) >> 16);

		kiv_os::TThread_Proc program = kiv_os::TThread_Proc(GetProcAddress(User_Programs, reinterpret_cast<char*>(regs.rdx.r)));
		pcb_lock.lock();
		memcpy_s(pm.pcb[pid].program_name, PROGRAM_NAME_SIZE, reinterpret_cast<char*>(regs.rdx.r), PROGRAM_NAME_SIZE);
		file_entry fe_in;
		fe_in.open = true;
		fe_in.file_descriptor = stdin_handle;
		pm.pcb[pid].file_table.push_back(fe_in);

		file_entry fe_out;
		fe_out.open = true;
		fe_out.file_descriptor = stdout_handle;

		pm.pcb[pid].file_table.push_back(fe_out);
		pm.pcb[pid].pid = pid;
		if (regs.rdi.r) {
			memcpy_s(pm.pcb[pid].arguments, ARGUMENTS_SIZE, reinterpret_cast<char*>(regs.rdi.r), ARGUMENTS_SIZE);
		}
		pm.pcb[pid].state = Process_State::NEW;
		pm.pcb[pid].registers = std::move(regs);
		pm.pcb[pid].thread_count = 0;
		pm.pcb[pid].registers.rax.r = stdin_handle;
		pm.pcb[pid].registers.rbx.r = stdout_handle;
		pcb_lock.unlock();
		uint16_t TID = Create_Thread((uint64_t)(&program), (uint64_t)(&pid));
		if (TID == ERROR_FLAG) {
			pcb_lock.lock();
			pm.pcb[pid].pid = EMPTY_ENTRY;
			pcb_lock.unlock();
			return ERROR_FLAG;
		}

		pcb_lock.lock();
		pm.pcb[pid].threads.push_back(TID);
		pcb_lock.unlock();

		return pid;
	}

	uint16_t process_manager::allocate_free_tid() {
		for (int i = 0; i < MAX_THREAD_COUNT; i++) {
			if (pm.tcb[i].id == EMPTY_ENTRY) {
				pm.tcb[i].id = i;
				return MAX_PROCESS_COUNT + i;
			}
		}
		return EMPTY_ENTRY;
	}

	int Create_Thread(uint64_t entry_pointer, uint64_t data_pointer) {
		pcb_lock.lock();
		kiv_os::TThread_Proc program = *(kiv_os::TThread_Proc*) entry_pointer;
		kiv_os::THandle PID;
		if (data_pointer != -1) {
			PID = *(kiv_os::THandle*) data_pointer;
		} else {
			PID = pm.allocate_free_pid();
		}

		if (!program) {
			pcb_lock.unlock();
			return ERROR_FLAG;
		}
		tcb_lock.lock();
		kiv_os::THandle TID = pm.allocate_free_tid();
		if (TID == EMPTY_ENTRY) {
			pcb_lock.unlock();
			tcb_lock.unlock();
			return ERROR_FLAG;
		}


		kiv_hal::TRegisters regs_2 = std::move(pm.pcb[PID].registers);
		uint8_t index = TID - MAX_PROCESS_COUNT;
		
		pm.tcb[index].id = TID;
		pm.tcb[index].pid = PID;
		signal_m.lock();
		threads++;

		signal_m.unlock();
		pm.tcb[index].thread = std::thread(Entry_point, program, regs_2, TID);
		tcb_lock.unlock();
		pm.pcb[PID].state = Process_State::RUNNING;
		pm.pcb[PID].thread_count++;
		pcb_lock.unlock();
		return TID;
	}

	kiv_os::NOS_Error Wait_For(kiv_hal::TRegisters &regs) {
		kiv_os::THandle* handles = (kiv_os::THandle*) regs.rdx.r;
		uint64_t handles_lenght = regs.rcx.r;
		kiv_os::THandle process = Get_Thread_PID();

		if (process == EMPTY_ENTRY) {
			std::unique_lock<std::mutex> uniq_lock(kernel_lock);
			kernel.wait(uniq_lock);
			return kiv_os::NOS_Error::Success;
		} else {
			pcb_lock.lock();
			pm.pcb[process].state = Process_State::WAITING;
			pcb_lock.unlock();
			for (uint64_t i = 0; i < handles_lenght; i++) {
				pcb_lock.lock();
				uint16_t index = (uint16_t)handles[i];

				if (index >= 0) {
					if (index < MAX_PROCESS_COUNT) {
						pm.pcb[index].parent_id = process;
						pm.pcb[process].waiting_for_counter++;
					} else if (index < (MAX_PROCESS_COUNT + MAX_THREAD_COUNT)) {
						index = index - MAX_PROCESS_COUNT;
						tcb_lock.lock();
						pm.tcb[index].to_notify = process;
						tcb_lock.unlock();
						pm.pcb[process].waiting_for_counter++;
					}
				} else {
					pcb_lock.unlock();
					return kiv_os::NOS_Error::Unknown_Error;
				}
				pcb_lock.unlock();
			}

			while (pm.pcb[process].waiting_for_counter > 0) {
				std::unique_lock<std::mutex> lck(pm.pcb[process].synch_lock);
				pm.pcb[process].synch_var.wait(lck);
				regs.rax.r = signalized;
			}


			
			return kiv_os::NOS_Error::Success;
		}
	}
	std::string state_to_char(kiv_os_process::Process_State state) {
		std::string s;
		switch (state) {
		case kiv_os_process::Process_State::READY: {s.append("READY"); break; }
		case kiv_os_process::Process_State::RUNNING: {s.append("RUNNING"); break; }
		case kiv_os_process::Process_State::TERMINATED: {s.append("TERMINATED"); break; }
		case kiv_os_process::Process_State::WAITING: {s.append("WAITING"); break; }
		}
		return s;
	}

	//write process info to buffer
	kiv_os::NOS_Error Write_Procfs(char *buffer, size_t buffer_size, size_t *actual_written_bytes) {
		std::string s;

		for (kiv_os::THandle process_pid = 0; process_pid < MAX_PROCESS_COUNT; process_pid++) {
			s.clear();
			pcb_lock.lock();
			if (pm.pcb[(uint16_t)process_pid].pid != EMPTY_ENTRY && pm.pcb[(uint16_t)process_pid].state != kiv_os_process::Process_State::READY && pm.pcb[(uint16_t)process_pid].state != kiv_os_process::Process_State::TERMINATED) {
				s.append(std::to_string(pm.pcb[(uint16_t)process_pid].pid));
				s.append(" | ");
				s.append(pm.pcb[(uint16_t)process_pid].program_name);
				s.append(" | ");
				s.append(std::to_string(pm.pcb[(uint16_t)process_pid].thread_count));
				s.append(" | ");
				s.append(state_to_char(pm.pcb[(uint16_t)process_pid].state));
				s.append("\n");
				memcpy_s(buffer + *actual_written_bytes, s.length(), s.c_str(), s.length());
				*actual_written_bytes += s.length();
			}
			pcb_lock.unlock();
		}

		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error Read_Exit_Code(kiv_hal::TRegisters& regs) {

		if (Get_Thread_PID() > MAX_PROCESS_COUNT) {
			uint16_t index = Get_Thread_PID() - MAX_PROCESS_COUNT;
			tcb_lock.lock();
			pm.tcb[index].id = -1;
			pm.tcb[index].used = false;
			regs.rcx.r = pm.tcb[index].return_value;
			tcb_lock.unlock();
		}
		else {
			if (pm.pcb[Get_Thread_PID()].used && pm.pcb[Get_Thread_PID()].pid != EMPTY_ENTRY && pm.pcb[Get_Thread_PID()].thread_count <= 0) {
				pcb_lock.lock();

				tcb_lock.lock();
				for (int16_t handle_tcb : pm.pcb[Get_Thread_PID()].threads) {
					uint16_t index_tcb = handle_tcb - MAX_PROCESS_COUNT;
					pm.tcb[index_tcb].id = EMPTY_ENTRY;
					pm.tcb[index_tcb].pid = EMPTY_ENTRY;
					pm.tcb[index_tcb].used = false;
				}
				tcb_lock.unlock();

				pm.pcb[Get_Thread_PID()].pid = EMPTY_ENTRY;
				pm.pcb[Get_Thread_PID()].used = false;
				pm.pcb[Get_Thread_PID()].state = kiv_os_process::Process_State::READY;
				regs.rcx.r = pm.pcb[Get_Thread_PID()].return_value;
				pcb_lock.unlock();
			}
			else {
				regs.rcx.x = -1;
				return kiv_os::NOS_Error::Unknown_Error;
			}
		}

		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error Exit(kiv_hal::TRegisters& regs) {
		kiv_os::THandle handle = (kiv_os::THandle) regs.rdx.r;
			if (pm.pcb[handle].pid == EMPTY_ENTRY) {
				return kiv_os::NOS_Error::Unknown_Error;
			}
			else {
				pcb_lock.lock();

				tcb_lock.lock();
				for (int16_t handle_tcb : pm.pcb[handle].threads) {
					uint16_t index_tcb = handle_tcb - MAX_PROCESS_COUNT;
					pm.tcb[index_tcb].id = EMPTY_ENTRY;
					pm.tcb[index_tcb].pid = EMPTY_ENTRY;
					pm.tcb[index_tcb].used = false;
				}
				tcb_lock.unlock();

				pm.pcb[handle].pid = EMPTY_ENTRY;
				pm.tcb[handle].used = false;
				regs.rcx.r = pm.pcb[handle].return_value;
				pcb_lock.unlock();
			}

		return kiv_os::NOS_Error::Success;
	}

	void End_Process(kiv_os::THandle TID) {
		for (int i = 1; i < MAX_PROCESS_COUNT; i++) {
			if (pm.tcb[TID].pid == i || pm.tcb[TID].pid == EMPTY_ENTRY) {
				continue;
			} else {
				pm.pcb[i].waiting_for_counter = 0;
				for (int j = 0; j < pm.pcb[i].threads.size(); j++) {
					kiv_os::THandle thread_handle = pm.pcb[i].threads[j];
					if (pm.tcb[thread_handle - MAX_PROCESS_COUNT].thread.joinable()) {
						tcb_lock.lock();
						End_thread(thread_handle, 0);
						tcb_lock.unlock();
					}
				}
			}
		}
	}

	kiv_os::NOS_Error Shutdown(kiv_hal::TRegisters& regs) {
		std::thread::id thread_id = std::this_thread::get_id();
		tid_map_lock.lock();
		kiv_os::THandle TID = tid_to_thread.at(thread_id);
		tid_map_lock.unlock();

		for (int i = MAX_PROCESS_COUNT - 1; i > 0; i--) {
			if (pm.tcb[i].pid != EMPTY_ENTRY) {
				Close_Handles(pm.pcb[i].file_table);
			}
		}

		End_Process(TID);

		pm.pcb[0].waiting_for_counter = 0;
		pm.pcb[0].synch_var.notify_all();

		return kiv_os::NOS_Error::Success;
	}

	kiv_os::NOS_Error Register_Signal_Handler(kiv_hal::TRegisters& regs) {
		tid_map_lock.lock();
		pcb_lock.lock();
		tcb_lock.lock();

		kiv_os::NSignal_Id signal = static_cast<kiv_os::NSignal_Id>(regs.rcx.r);
		kiv_os::TThread_Proc process_handle = reinterpret_cast<kiv_os::TThread_Proc>(regs.rdx.r);
		kiv_os::THandle thread_id = Get_Thread_PID();

		tcb_lock.unlock();
		pcb_lock.unlock();
		tid_map_lock.unlock();

		return kiv_os::NOS_Error::Success;
	}
}
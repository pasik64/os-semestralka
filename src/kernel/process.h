#pragma once

#include <vector>
#include <thread>
#include <mutex>
#include <map>
#include "kernel.h"
#include "io.h"
#include <Windows.h>
#include <array>

#define MAX_PROCESS_COUNT 1024
#define MAX_THREAD_COUNT 1024
#define EMPTY_ENTRY 2048
#define ERROR_FLAG  2049
#define ARGUMENTS_SIZE  256

namespace kiv_os_process {
	static const char* PROC_ADDRESS = "PROC_ADDRESS";
	static const uint8_t PROGRAM_NAME_SIZE = 16;

	//enums
	enum Process_State {
		NEW,
		READY,
		RUNNING,
		WAITING,
		TERMINATED
	};

	//structs
	struct file_entry {
		bool open = false;
		kiv_os::THandle file_descriptor = ERROR_FLAG;
	};
	struct PCB {
		int32_t pid = EMPTY_ENTRY;
		Process_State state = Process_State::READY;
		int32_t thread_count;
		int32_t parent_id = EMPTY_ENTRY;
		char program_name[PROGRAM_NAME_SIZE];
		char arguments[256];
		kiv_hal::TRegisters registers;
		std::vector<kiv_os::THandle> threads = {};
		std::vector<file_entry> file_table;
		uint16_t return_value;
		uint16_t waiting_for_counter = 0;
		bool used;
		std::condition_variable synch_var;
		std::mutex synch_lock;
	};
	struct TCB {
		int32_t id = EMPTY_ENTRY;
		int32_t pid = EMPTY_ENTRY;
		kiv_os::THandle to_notify = EMPTY_ENTRY;
		Process_State state;
		std::thread thread;
		uint16_t return_value;
		bool used;
	};
	struct process_manager {
		std::array<TCB, MAX_THREAD_COUNT> tcb;
		std::array<PCB, MAX_PROCESS_COUNT> pcb;
		kiv_hal::TRegisters regs{ 0 };
		uint16_t allocate_free_tid();
		uint16_t allocate_free_pid();
	};

	//functions
	void Handle_Process(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Clone(kiv_hal::TRegisters& regs);
	int Create_Process(kiv_hal::TRegisters& regs);
	int Create_Thread(uint64_t entry_pointer, uint64_t data_pointer);
	void End_thread(kiv_os::THandle TID, uint16_t result);
	void Close_Handles(std::vector<file_entry> handles);
	void Notify_Process(kiv_os::THandle PID, uint16_t result);
	kiv_os::THandle Get_Thread_PID();
	kiv_os::NOS_Error Wait_For(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Read_Exit_Code(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Exit(kiv_hal::TRegisters& regs);
	void End_Process(kiv_os::THandle TID);
	kiv_os::NOS_Error Shutdown(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Register_Signal_Handler(kiv_hal::TRegisters& regs);
	kiv_os::NOS_Error Write_Procfs(char* buffer, size_t buffer_size, size_t* actual_written_bytes);
}

#include "command.h"


kiv_os_command::command new_command(std::string program, std::vector<std::string> params, 
	std::string in_file, std::string out_file, kiv_os_command::Type in_type, kiv_os_command::Type out_type) {
	
	kiv_os_command::command new_command;
	
	new_command.program = program;
	new_command.params = params;
	new_command.in_file = in_file;
	new_command.out_file = out_file;
	new_command.in_type = in_type;
	new_command.out_type = out_type;

	return new_command;
}

std::vector<kiv_os_command::command> kiv_os_command::parse_commands(char *buffer, size_t buffer_size) {

	std::vector<std::string> commands_str;
	size_t commands_str_size;
	std::string part;
	std::vector<kiv_os_command::command> commands;
	std::string program;
	std::vector<std::string> params;
	std::string in_file;
	std::string out_file;
	kiv_os_command::Type in_type;
	kiv_os_command::Type out_type;

	program = "\0";
	in_file = "\0";
	out_file = "\0";
	in_type = Type::STANDARD;
	out_type = Type::STANDARD;

	if (buffer_size == 0) {
		return commands;
	}
	
	commands_str = kiv_os_command_parser::split_line(buffer, buffer_size);

	commands_str_size = commands_str.size();

	bool command_read = false;

	for (size_t i = 0; i < commands_str_size; i++) {
		part = commands_str[i];

		if (part == "echo" || part == "cd" || part == "dir" || part == "md" || part == "rd" || part == "type" || part == "find" 
			|| part == "sort" || part == "tasklist" ||  part == "rgen" || part == "freq" || part == "exit" || part == "shell") {

			command_read = true;
			if ((i != 0) && (program != "\0")) {
				commands.push_back(new_command(program, params, in_file, out_file, in_type, out_type));

				if (out_type == Type::PIPE) {
					in_type = Type::PIPE;
				}
				else {
					in_type = Type::STANDARD;
				}

				out_type = Type::STANDARD;
			}
			
			program = part;
			params.clear();
			in_file = "\0";
			out_file = "\0";

		}
		else if (part == "shutdown") {
			commands.clear();
			in_type = Type::STANDARD;
			out_type = Type::STANDARD;
			params.clear();
			commands.push_back(new_command(part, params, in_file, out_file, in_type, out_type));
			return commands;
		}
		else if (part == "|") {
			out_type = Type::PIPE;
			command_read = false;
		}
		else if (part == ">" || part == "1>") {
			out_type = Type::FILE;

			if (i + 1 < commands_str_size) {
				i++;
				out_file = commands_str[i];
			}
		}
		else if (part == ">>" || part == "1>>") {
			out_type = Type::FILE_APPEND;

			if (i + 1 < commands_str_size) {
				i++;
				out_file = commands_str[i];
			}
		}
		else if (part == "<") {
			in_type = Type::FILE;

			if (i + 1 < commands_str_size) {
				i++;
				in_file = commands_str[i];
			}
		}
		else {
			if (command_read) {
				params.push_back(part);
			}
			else {
				commands.clear();
				return commands;
			}
		}
	}

	if (commands_str_size > 0) {
		commands.push_back(new_command(program, params, in_file, out_file, in_type, out_type));
	}

	return commands;
}
#pragma once

#include "command_parser.h"

namespace kiv_os_command {

	enum class Type {
		STANDARD,
		PIPE,
		FILE,
		FILE_APPEND
	};

	typedef struct Command {
		std::string program;
		std::vector<std::string> params;
		std::string in_file;
		std::string out_file;

		Type in_type = Type::STANDARD;
		Type out_type = Type::STANDARD;

		kiv_os::THandle in_handle = 0;
		kiv_os::THandle out_handle = 1;

	} command;


	std::vector<command> parse_commands(char *buffer, size_t buffer_size);
}
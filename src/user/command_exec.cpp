#pragma once

#include "command_exec.h"
#include "errors.h"
#include "rtl.h"

bool validate_program_name(std::string program_name) {
	if (program_name.empty()) {
		return false;
	}

	return true;
}

void print_wrong_syntax(std::string comment, kiv_os::THandle out) {
	size_t counter_out;
	kiv_os_rtl::Write_File(out, comment.c_str(), comment.length(), counter_out);
}

void print_wrong_syntax(const kiv_os::THandle out) {
	std::string incorrect_syntax = "The syntax of the command is incorrect.\n";
	print_wrong_syntax(incorrect_syntax, out);
}

void change_working_dir(kiv_os_command::command command, kiv_os::THandle out) {
	if (command.params.size() != 1) {
		size_t counter;
		kiv_os_rtl::Write_File(out, "File not found\n", sizeof("File not found\n"), counter);
		return;
	}

	if (!kiv_os_rtl::Set_Working_Dir(command.params.at(0).c_str())) {
		kiv_os_error::process_error(kiv_os_rtl::Last_Error, out);
	}
}

std::string merge_params(std::vector<std::string> parameters) {
	std::string merged_params = "";

	for (int i = 0; i < (int)parameters.size(); i++) {
		if (i != 0) {
			merged_params.append(" ");
		}

		merged_params.append(parameters[i]);
	}

	return merged_params;
}

void wait_for_processes(kiv_os::THandle *processes, int processes_count) {
	kiv_os::THandle signalised;
	uint16_t exit_code;

	if (kiv_os_rtl::Wait_For(processes, processes_count, signalised)) {
		for (int i = 0; i < processes_count && i != signalised; i++) {
			kiv_os_rtl::Read_Exit_Code(processes[i], exit_code);
		}
		kiv_os_rtl::Read_Exit_Code(signalised, exit_code);
	}
}

bool run_commands(std::vector<kiv_os_command::command> *commands, const kiv_os::THandle std_in, const kiv_os::THandle std_out) {
	kiv_os_command::command current_command;
	std::string prepared_params;
	kiv_os::THandle created_process;
	kiv_os::THandle processes[kiv_os_command_exec::processes_count];

	for (int i = 0; i < (int)(*commands).size(); i++) {
		current_command = (*commands)[i];

		if (current_command.program == "exit") {
			return false; //ukon�� se shell
		}

		if (current_command.program == "cd") {
			change_working_dir(current_command, std_out);
			return true; //n�vrat, ale nech�me shell b�et
		}

		prepared_params = merge_params(current_command.params);

		if (!kiv_os_rtl::Create_Process(current_command.program.c_str(), prepared_params.c_str(), current_command.in_handle, current_command.out_handle, created_process)) {
			//kiv_os_error::process_error(kiv_os_rtl::Last_Error, std_out);
			//return true; //n�vrat, ale nech�me shell b�et
		}

		processes[i] = created_process;
	}

	wait_for_processes(processes, (int)(*commands).size());

	return true;
}

bool set_in_handle(kiv_os_command::Type in_type, kiv_os::THandle *pipe, std::string in_file, kiv_os::THandle *in_handle, const kiv_os::THandle std_in, const kiv_os::THandle std_out) {

	if (in_type == kiv_os_command::Type::STANDARD) {
		*in_handle = std_in;
	}
	else if (in_type == kiv_os_command::Type::PIPE) {
		*in_handle = pipe[1];

		if (*in_handle == NULL) {
			print_wrong_syntax(std_out);
			return false;
		}

	}
	else if (in_type == kiv_os_command::Type::FILE) {
		if (!in_file.empty()) {
			if (!kiv_os_rtl::Open_File(in_file.c_str(), (uint8_t)kiv_os::NOpen_File::fmOpen_Always, 0, *in_handle)) {
				kiv_os_error::process_error(kiv_os_rtl::Last_Error, std_out);
				return false;
			}
		}
		else {
			print_wrong_syntax(std_out);
			return false;
		}
	}

	return true;
}

bool set_out_handle(kiv_os_command::Type out_type, kiv_os::THandle *pipe, std::string out_file, kiv_os::THandle *out_handle, const kiv_os::THandle std_out) {

	if (out_type == kiv_os_command::Type::STANDARD) {
		*out_handle = std_out;
	}
	else if (out_type == kiv_os_command::Type::PIPE) {
		if (kiv_os_rtl::Create_Pipe(pipe)) {
			*out_handle = pipe[0];
		}
		else {
			print_wrong_syntax("Unable to create pipe.\n", std_out);
			return false;
		}
	}
	else if (out_type == kiv_os_command::Type::FILE) {
		if (!out_file.empty()) {
			if (!kiv_os_rtl::Open_File(out_file.c_str(), 0, 0, *out_handle)) {
				kiv_os_error::process_error(kiv_os_rtl::Last_Error, std_out);
				return false;
			}
		}
		else {
			print_wrong_syntax(std_out);
			return false;
		}
	}
	else if (out_type == kiv_os_command::Type::FILE_APPEND) {
		if (!out_file.empty()) {
			if (kiv_os_rtl::Open_File(out_file.c_str(), (uint8_t)kiv_os::NOpen_File::fmOpen_Always, 0, *out_handle)) {
				uint64_t position = 0;

				if (!kiv_os_rtl::Seek(*out_handle, kiv_os::NFile_Seek::Set_Position, kiv_os::NFile_Seek::End, position)) {
					kiv_os_error::process_error(kiv_os_rtl::Last_Error, std_out);
					return false;
				}
			}
			else {
				if (!kiv_os_rtl::Open_File(out_file.c_str(), 0, 0, *out_handle)) {
					kiv_os_error::process_error(kiv_os_rtl::Last_Error, std_out);
					return false;
				}
			}
		}
		else {
			print_wrong_syntax(std_out);
			return false;
		}
	}

	return true;
}

bool set_handles(std::vector<kiv_os_command::command> *commands, const kiv_os::THandle std_in, const kiv_os::THandle std_out) {
	kiv_os::THandle pipe[2];
	kiv_os_command::command *current_command;
	std::string wrong_command = "\0";
	size_t counter_in = -1;
	size_t counter_out = -1;
	kiv_os::THandle in_handle;
	kiv_os::THandle out_handle;

	for (int i = 0; i < (int)(*commands).size(); i++) {
		current_command = &(*commands)[i];

		if (validate_program_name(current_command->program)) {

			if (!(set_in_handle(current_command->in_type, pipe, current_command->in_file, &in_handle, std_in, std_out) 
				&& set_out_handle(current_command->out_type, pipe, current_command->out_file, &out_handle, std_out))) {
				return false; //shell st�le pob��, ale ukon�� se vykon�v�n� p��kaz�
			}

			current_command->in_handle = in_handle;
			current_command->out_handle = out_handle;

		}
		else {
			wrong_command = "\'" + current_command->params[0] + "\' is not recognized as an internal or external command.";
			kiv_os_rtl::Write_File(std_out, wrong_command.c_str(), wrong_command.length(), counter_out);
			return false;
		}
	}

	return true;
}

bool kiv_os_command_exec::exec_commands(std::vector<kiv_os_command::command> *commands, const kiv_os::THandle std_in, const kiv_os::THandle std_out) {

	if (!set_handles(commands, std_in, std_out)) {
		return true; //shell nech�me b�et
	}

	if (!run_commands(commands, std_in, std_out)) {
		return false;
	}

	return true;
}
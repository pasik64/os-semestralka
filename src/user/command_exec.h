#pragma once

#include "command.h"

namespace kiv_os_command_exec {

	const uint8_t processes_count = 64;

	bool exec_commands(std::vector<kiv_os_command::command> *commands, const kiv_os::THandle std_in, const kiv_os::THandle std_out);

}
#include <cctype>
#include "command_parser.h"

kiv_os_command_parser::program_params new_program_params(std::string program, std::vector<std::string> params,
	int params_count, kiv_os::THandle in_handle, kiv_os::THandle out_handle) {

	kiv_os_command_parser::program_params new_program_params;

	new_program_params.program = program;
	new_program_params.params = params;
	new_program_params.params_count = params_count;
	new_program_params.in_handle = in_handle;
	new_program_params.out_handle = out_handle;

	return new_program_params;
}

std::vector<std::string> remove_whitespaces(std::vector<std::string> input) {
	int spaces_count = 0;
	const char* word;
	std::vector<std::string> without_whitespaces;

	for (int i = 0; i < (int)input.size(); i++) {
		word = input[i].c_str();
		spaces_count = 0;

		for (int j = 0; j < (int)input[i].length(); j++) {
			if (isspace((unsigned char)word[j])) {
				spaces_count++;
			}
		}

		if (spaces_count != input[i].length()) {
			without_whitespaces.push_back(input[i]);
		}
	}

	return without_whitespaces;
}

std::vector<std::string> kiv_os_command_parser::split_line(char* line, size_t line_size) {
	std::vector<std::string> splitted_line;
	std::string part = "";
	bool quotes = false;

	for (size_t i = 0; i < line_size; i++) {
		if (!quotes) {
			if (line[i] == ' ' || line[i] == '<' || line[i] == '>' || line[i] == '|') {
				splitted_line.push_back(part);
				if (line[i] == '<' || line[i] == '|' || line[i] == '>') {
					std::string redirect_part = "";
					redirect_part.append(1, line[i]);
					if (line[i] == '>' && line[i + 1] == '>') {
						redirect_part.append(1, line[i + 1]);
						i++;
					}
					splitted_line.push_back(redirect_part);
				}
				part = "";
			}
			else if (isspace((unsigned char)line[i]) && (line[i] != ' ')) {
				continue;
			}
			else {
				if (line[i] == '\"')
					quotes = true;
				part.append(1, line[i]);
			}
		}
		else {
			if (isspace((unsigned char)line[i]) && (line[i] != ' ')) {
				continue;
			}
			else {
				if (line[i] == '\"')
					quotes = false;
				part.append(1, line[i]);
			}

		}

	}

	splitted_line.push_back(part);

	return remove_whitespaces(splitted_line);
}

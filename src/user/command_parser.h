#pragma once

#include <string>
#include <vector>
#include "../api/api.h"

namespace kiv_os_command_parser {

	typedef struct Program_params {
		std::string program;
		std::vector<std::string> params;
		int params_count = 0;

		kiv_os::THandle in_handle = 0;
		kiv_os::THandle out_handle = 1;

	} program_params;

	std::vector<std::string> split_line(char *line, size_t line_size);
}
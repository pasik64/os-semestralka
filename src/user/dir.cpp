#include "dir.h"

size_t __stdcall dir(const kiv_hal::TRegisters& regs) {
    const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
    const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);
    char* args = reinterpret_cast<char*>(regs.rdi.r), buffer[kiv_os_rtl::buffer_size];
    std::string args_string = std::string(args);
    size_t position = 0, read, current_pos = 0, written;
    int i = 0, file_counter = 0, directory_counter = 0;
    kiv_os::TDir_Entry tdir;

    std::vector<char*>args_vector;
    char* temp;
    char* context = NULL;


    temp = strtok_s(args, " ", &context);
    while (temp != nullptr){
        args_vector.insert(args_vector.end() - i, temp);
        temp = strtok_s(nullptr, " ", &context);
        i++;
    }


    bool s_present = false;
    std::string dir_locations;
    for (std::string token : args_vector) {
        if (token.compare("/s") == 0 || token.compare("/S") == 0) {
            s_present = true;
        } else {
            dir_locations = token;
        }
    }

    std::vector<kiv_os::TDir_Entry> directories;
    std::vector<kiv_os::TDir_Entry> files;

    kiv_os::THandle handle;


    if (dir_locations.empty()) {
        size_t counter = 0;
        kiv_os_rtl::Get_Working_Dir(buffer, kiv_os_rtl::buffer_size, counter);
        dir_locations = std::string(buffer);
    }

    if (!kiv_os_rtl::Open_File(dir_locations.c_str(), (uint8_t)kiv_os::NOpen_File::fmOpen_Always, (uint8_t)kiv_os::NFile_Attributes::Directory, handle)) {
        kiv_os_rtl::Write_File(std_out, "File not found\n", sizeof("File not found\n"), written);
        return (uint16_t)kiv_os::NOS_Error::File_Not_Found;
    }

    if (!kiv_os_rtl::Read_File(handle, buffer, kiv_os_rtl::buffer_size, read)) {
        return (uint16_t)kiv_os::NOS_Error::IO_Error;
    }

    while (current_pos < read) {
        memcpy_s(&tdir, sizeof(kiv_os::TDir_Entry), &buffer[current_pos], sizeof(kiv_os::TDir_Entry));

        if (tdir.file_attributes & (uint8_t)kiv_os::NFile_Attributes::Directory) {
            directories.push_back(tdir);
            directory_counter++;
        } else {
            files.push_back(tdir);
            file_counter++;
        }
        current_pos += sizeof(kiv_os::TDir_Entry);
    }



    for (kiv_os::TDir_Entry dir : directories) {
        std::string output("<DIR> ");
        output.append(dir.file_name);
        output.append("\n");
        kiv_os_rtl::Write_File(std_out, output.c_str(), output.size(), written);
    }

    for (kiv_os::TDir_Entry dir : files) {
        std::string output(dir.file_name);
        output.append("\n");
        kiv_os_rtl::Write_File(std_out, output.c_str(), output.size(), written);
    }

    if (s_present) {
        kiv_hal::TRegisters regs2 = regs;
        memcpy_s(&regs2, sizeof(kiv_hal::TRegisters), &regs, sizeof(kiv_hal::TRegisters));

        kiv_os_rtl::Write_File(std_out, "\n", sizeof("\n"), written);
        for (kiv_os::TDir_Entry dir_e : directories) {
            if (strcmp(dir_e.file_name, ".") && strcmp(dir_e.file_name, "..")) {
                std::string new_args(dir_locations);
                new_args.append("\\");
                new_args.append(dir_e.file_name);
                new_args.append(" /s");
                regs2.rdi.r = reinterpret_cast<uint64_t>(new_args.c_str());
                dir(regs2);
            }
        }
    }

    return (uint16_t)kiv_os::NOS_Error::Success;
}
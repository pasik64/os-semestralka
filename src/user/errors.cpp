#include "errors.h"

void print_err(char *message, kiv_os::THandle out_handle) {
	size_t counter;
	kiv_os_rtl::Write_File(out_handle, message, strlen(message), counter);
}

void kiv_os_error::process_error(kiv_os::NOS_Error error, kiv_os::THandle out_handle) {

	switch (error) {
	case kiv_os::NOS_Error::Invalid_Argument:
		print_err("Invalid argument", out_handle);
		break;
	case kiv_os::NOS_Error::File_Not_Found:
		print_err("File not found", out_handle);
		break;
	case kiv_os::NOS_Error::Directory_Not_Empty:
		print_err("Folder is not empty", out_handle);
		break;
	case kiv_os::NOS_Error::Not_Enough_Disk_Space:
		print_err("Not enough disk space", out_handle);
		break;
	case kiv_os::NOS_Error::Out_Of_Memory:
		print_err("Out of memory", out_handle);
		break;
	case kiv_os::NOS_Error::Permission_Denied:
		print_err("Permission denied", out_handle);
		break;
	case kiv_os::NOS_Error::IO_Error:
		print_err("IO error", out_handle);
		break;
	case kiv_os::NOS_Error::Unknown_Error:
		print_err("Unknown error", out_handle);
		break;
	}
}
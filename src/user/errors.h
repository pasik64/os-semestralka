#pragma once

#include "rtl.h"

namespace kiv_os_error {
	void process_error(kiv_os::NOS_Error error, kiv_os::THandle error_handle);
}
#include "find.h"

size_t __stdcall find(const kiv_hal::TRegisters& regs) {
	kiv_os::THandle in = static_cast<kiv_os::THandle>(regs.rax.x);
	kiv_os::THandle out = static_cast<kiv_os::THandle>(regs.rbx.x);
	char* argumenty = reinterpret_cast<char*>(regs.rdi.r);

	size_t counter = 0;
	std::stringstream stream;
	std::string token;
	std::vector<std::string> tokens;

	stream << argumenty;
	while (std::getline(stream, token, ' ')) {
		tokens.push_back(token);
	}

		bool v_present = false;
		bool c_present = false;
		std::string searched;
		std::vector<std::string> search_locations;
		std::vector<std::string> lines;

		for (std::string tok : tokens) {
			if (tok.compare("/v") == 0 || tok.compare("/V") == 0) {
				v_present = true;
			} else if (tok.compare("/c") == 0 || tok.compare("/C") == 0) {
				c_present = true;
			} else if (tok.front() == '"' && tok.back() == '"' && tok.length() > 2){
				searched = tok.substr(1, tok.length()-2);
			} else {
				search_locations.push_back(tok);
			}
		}

		if (searched.empty()) {
			kiv_os_rtl::Write_File(out, "Command was called with invalid arguments\n", sizeof("Command was called with invalid arguments\n"), counter);
			return (size_t)kiv_os::NOS_Error::Invalid_Argument;
		}

		if (search_locations.empty()) {
			size_t counter_in;
			size_t counter_out;
			int found_counter = 0;
			char buffer[kiv_os_rtl::fat_buffer_size];
			counter=0;
			std::string result;
			do {
				memset(buffer, 0, kiv_os_rtl::fat_buffer_size);
				if (!kiv_os_rtl::Read_File(in, buffer, kiv_os_rtl::fat_buffer_size, counter_in)) {
					return (size_t)kiv_os::NOS_Error::IO_Error;
				}
				
				if (strcmp(buffer, "")) {
					lines.push_back(buffer);
				}
			} while (!(counter_in == 0 || counter_in == -1));

			if (c_present) {
				for (std::string line : lines) {
					if (v_present) {
						if (line.find(searched) == std::string::npos) {
							found_counter += 1;
						}
					}
					else {
						if (line.find(searched) != std::string::npos) {
							found_counter += 1;
						}
					}
				}
				result.append(std::to_string(found_counter) + "\n");
			}
			else {
				for (std::string line : lines) {
					if (v_present) {
						if (line.find(searched) == std::string::npos) {
							result.append(line + "\n");
						}
					}
					else {
						if (line.find(searched) != std::string::npos) {
							result.append(line + "\n");

						}
					}
				}
			}

			if (!kiv_os_rtl::Write_File(out, result.c_str(), result.length(), counter_out)) {
				return (size_t)kiv_os::NOS_Error::IO_Error;
			}
			return (size_t)kiv_os::NOS_Error::Success;

		} else {
			kiv_os::THandle handle;
			size_t counter_in;
			size_t counter_out;
			for (std::string search_loc : search_locations) {
				std::string result;
				size_t position = 0;
				int found_counter = 0;
				if (!kiv_os_rtl::Open_File(search_loc.c_str(), (uint8_t)kiv_os::NOpen_File::fmOpen_Always, 0, handle)) {
					return (size_t)kiv_os::NOS_Error::IO_Error;
				}

				if (!kiv_os_rtl::Seek(handle, kiv_os::NFile_Seek::Set_Position, kiv_os::NFile_Seek::Beginning, position)) {
					return (size_t)kiv_os::NOS_Error::IO_Error;
				}

				char buffer[kiv_os_rtl::fat_buffer_size];
				do {
					memset(buffer, 0, kiv_os_rtl::fat_buffer_size);
					if (!kiv_os_rtl::Read_File(handle, buffer, kiv_os_rtl::fat_buffer_size, counter_in)) {
						return (size_t)kiv_os::NOS_Error::IO_Error;
					}
					
					if (strcmp(buffer, "")) {
						lines.push_back(buffer);
					}
				} while (!(counter_in == 0 || counter_in == -1));

				if (c_present) {
					for (std::string line : lines) {
						if (v_present) {
							if (line.find(searched) == std::string::npos) {
								found_counter += 1;
							}
						} else {
							if (line.find(searched) != std::string::npos) {
								found_counter += 1;
							}
						}
					}
					result.append(std::to_string(found_counter) + "\n");
				} else {
					for (std::string line : lines) {
						if (v_present) {
							if (line.find(searched) == std::string::npos) {
								result.append(line + "\n");
							}
						} else {
							if (line.find(searched) != std::string::npos) {
								result.append(line + "\n");
							}
						}
					}
				}

				if (!kiv_os_rtl::Write_File(out, result.c_str(), result.length(), counter_out)) {
					return (size_t)kiv_os::NOS_Error::IO_Error;
				}
			}

		return (size_t)kiv_os::NOS_Error::Success;
		}
}
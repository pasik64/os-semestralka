#include "freq.h"

size_t __stdcall freq(const kiv_hal::TRegisters& regs) {
	kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	char* tokens = reinterpret_cast<char*>(regs.rdi.r);
	std::stringstream stream(tokens);
	std::string token;
	std::vector<std::string> parameters;

	size_t counter = 0;

	while (std::getline(stream, token, ' ')) {
		parameters.push_back(token);
	}

	if (parameters.size()) {
		kiv_os_rtl::Write_File(out_handle, "Command was called with invalid arguments\n", sizeof("Command was called with invalid arguments\n"), counter);
		return (size_t)kiv_os::NOS_Error::Invalid_Argument;
	} else {
		uint64_t freqency_table [FREQUENCY_TABLE_SIZE] { 0 };
		counter = 0;
		char buffer[kiv_os_rtl::fat_buffer_size];

		while (kiv_os_rtl::Read_File(in_handle, buffer, kiv_os_rtl::fat_buffer_size, counter)) {
			if (counter == 0 || buffer[0] == 0 || buffer[0] == 4) {
				break;
			}

			for (int i = 0; i < counter; i++) {
				freqency_table[buffer[i]] += 1;
			}
		}

		counter = 0;
		for (int i = 0; i < FREQUENCY_TABLE_SIZE; i++) {
			if (freqency_table[i]) {
				std::stringstream hexa_stream;
				hexa_stream << std::hex << (0xFF & i);
				std::string tableRow("0x" + hexa_stream.str() + " : " + std::to_string(freqency_table[i]) + "\n");
				if (kiv_os_rtl::Write_File(out_handle, tableRow.c_str(), tableRow.length(), counter) == 0) {
					return (size_t)kiv_os::NOS_Error::IO_Error;
				}
			}
		}

		return (size_t)kiv_os::NOS_Error::Success;
	}
}
#pragma once

#include "rtl.h"
#include "..\api\api.h"

#include <sstream>
#include <vector>
#include <algorithm>

#define FREQUENCY_TABLE_SIZE 256

extern "C" size_t __stdcall freq(const kiv_hal::TRegisters &regs);
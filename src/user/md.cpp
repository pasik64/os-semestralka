#include "md.h"

size_t __stdcall md(const kiv_hal::TRegisters &regs) {
    const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
    const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);
    char *file_name = reinterpret_cast<char*>(regs.rdi.r);
    int new_file_mode = 0;

    size_t counter = 0;
    kiv_os::THandle handle;

    if (!strcmp(file_name, "")) {
        kiv_os_rtl::Write_File(std_out, "Command was called with invalid arguments\n", sizeof("Command was called with invalid arguments\n"), counter);
        return (uint16_t)kiv_os::NOS_Error::Invalid_Argument;
    }

    if(!kiv_os_rtl::Open_File(file_name, new_file_mode, (uint8_t)kiv_os::NFile_Attributes::Directory, handle)){
        return (uint16_t)kiv_os::NOS_Error::IO_Error;
    }
    return (uint16_t)kiv_os::NOS_Error::Success;
}
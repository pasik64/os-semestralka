#pragma once

#include "..\api\api.h"
#include "rtl.h"

#include <iostream>
#include <string>

extern "C" size_t __stdcall md(const kiv_hal::TRegisters &regs);
#include "rd.h"

size_t __stdcall rd(const kiv_hal::TRegisters &regs) {
    const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
    const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);
    char *file_name = reinterpret_cast<char*>(regs.rdi.r), buffer[kiv_os_rtl::buffer_size];
    size_t read;
    size_t position = 0;
    size_t counter = 0;
    size_t files = 0;
    kiv_os::TDir_Entry dir;
    kiv_os::THandle handle;


    if (!strcmp(file_name, "")) {
        kiv_os_rtl::Write_File(std_out, "Command was called with invalid arguments\n", sizeof("Command was called with invalid arguments\n"), counter);
        return (uint16_t)kiv_os::NOS_Error::Invalid_Argument;
    }

    if(!kiv_os_rtl::Open_File(file_name, (uint8_t)kiv_os::NOpen_File::fmOpen_Always, (uint8_t)kiv_os::NFile_Attributes::Directory, handle)){
        kiv_os_rtl::Write_File(std_out, "File not found\n", sizeof("File not found\n"), counter);
        return (uint16_t)kiv_os::NOS_Error::File_Not_Found;
    }

    if (!kiv_os_rtl::Seek(handle, kiv_os::NFile_Seek::Set_Position, kiv_os::NFile_Seek::Beginning, position)) {
        return (size_t)kiv_os::NOS_Error::IO_Error;
    }

    if (!kiv_os_rtl::Read_File(handle, buffer, sizeof(kiv_os_rtl::buffer_size), read)){
    return (uint16_t)kiv_os::NOS_Error::IO_Error;
    }

    for (; position < read; position += sizeof kiv_os::TDir_Entry) {
        memcpy_s(&dir, sizeof kiv_os::TDir_Entry, &buffer[position], sizeof kiv_os::TDir_Entry);
        if (strcmp(dir.file_name, ".") && strcmp(dir.file_name, "..")) {
            files++;
        }
    }

    if(files != 0){
        kiv_os_rtl::Write_File(std_out, "Directory was not empty\n", sizeof("Directory was not empty\n"), counter);
        return (uint16_t)kiv_os::NOS_Error::Directory_Not_Empty;
    }

    kiv_os_rtl::Delete_File(file_name);
    return (uint16_t)kiv_os::NOS_Error::Success;
}
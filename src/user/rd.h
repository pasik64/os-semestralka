#pragma once

#include "../api/api.h"
#include "rtl.h"

#include <iostream>
#include <string>

extern "C" size_t __stdcall rd(const kiv_hal::TRegisters &regs);
#include "rgen.h"

std::atomic<bool> run = true;
kiv_os::THandle in_handle;
kiv_os::THandle out_handle;


size_t __stdcall rgen_start() {
	srand(static_cast<unsigned int>(time(NULL)));
	size_t counter = 0;
	while (run) {
		std::string rand_string = std::to_string(rand());
		rand_string.append("\n");
		if (!kiv_os_rtl::Write_File(out_handle, rand_string.c_str(), rand_string.length(), counter)) {
			return (size_t)kiv_os::NOS_Error::IO_Error;
		}
	}

	return (size_t)kiv_os::NOS_Error::Success;
}

void __stdcall rgen_end() {
	const int buffer_size = 1;
	char buffer[buffer_size];
	size_t counter;

	while (!kiv_os_rtl::Read_File(in_handle, buffer, buffer_size, counter)) {
		if ((in_handle == 0 && counter == 0) || buffer[0] == 4 || buffer[0] == 0) {
			break;
		}
	}

	run = false;
}

size_t __stdcall rgen(const kiv_hal::TRegisters& regs) {
	in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	size_t counter;
	char* tokens = reinterpret_cast<char*>(regs.rdi.r);
	std::stringstream stream(tokens);
	std::string token;
	std::vector<std::string> parameters;

	while (std::getline(stream, token, ' ')) {
		parameters.push_back(token);
	}

	if (parameters.size()) {
		kiv_os_rtl::Write_File(out_handle, "Command was called with invalid arguments\n", sizeof("Command was called with invalid arguments\n"), counter);
		return (size_t)kiv_os::NOS_Error::Invalid_Argument;
	} else {
		uint16_t retval;
		kiv_os::THandle signal_handle = 0;
		kiv_os::THandle handles[2];
		kiv_os::TThread_Proc program_0 = kiv_os::TThread_Proc(rgen_start);
		kiv_os::TThread_Proc program_1 = kiv_os::TThread_Proc(rgen_end);

		if (!kiv_os_rtl::Create_Thread(&program_0, (void *) -1, handles[0])) {
			run = false;
			return (size_t)kiv_os::NOS_Error::Unknown_Error;
		}

		if (!kiv_os_rtl::Create_Thread(&program_1, (void*) -1, handles[1])) {
			run = false;
			return (size_t)kiv_os::NOS_Error::Unknown_Error;
		}
		if (!kiv_os_rtl::Wait_For(handles, 2, signal_handle)) {
			run = false;
			return (size_t)kiv_os::NOS_Error::Unknown_Error;
		}
		if (!kiv_os_rtl::Read_Exit_Code(handles[0], retval)) {
			run = false;
			return (size_t)kiv_os::NOS_Error::Unknown_Error;
		}

		if (!kiv_os_rtl::Read_Exit_Code(handles[1], retval)) {
			run = false;
			return (size_t)kiv_os::NOS_Error::Unknown_Error;
		}
		run = true;

		return (size_t)kiv_os::NOS_Error::Success;
	}
}
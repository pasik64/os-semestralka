#pragma once

#include "..\api\api.h"
#include "rtl.h"

#include <sstream>
#include <vector>
#include <algorithm>

#include <time.h> 

extern "C" size_t __stdcall rgen(const kiv_hal::TRegisters & regs);
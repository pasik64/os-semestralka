#include "rtl.h"

std::atomic<kiv_os::NOS_Error> kiv_os_rtl::Last_Error;

kiv_hal::TRegisters Prepare_SysCall_Context(kiv_os::NOS_Service_Major major, uint8_t minor) {
	kiv_hal::TRegisters regs;
	regs.rax.h = static_cast<uint8_t>(major);
	regs.rax.l = minor;
	return regs;
}

void kiv_os_rtl::Default_Signal() {
	return;
}

bool kiv_os_rtl::Read_File(const kiv_os::THandle file_handle, char* const buffer, const size_t buffer_size, size_t &read) {
	kiv_hal::TRegisters regs =  Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Read_File));
	regs.rdx.r = static_cast<decltype(regs.rdx.r)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = buffer_size;	
	
	const bool result = kiv_os::Sys_Call(regs);
	read = regs.rax.r;
	return result;
}

bool kiv_os_rtl::Write_File(const kiv_os::THandle file_handle, const char *buffer, const size_t buffer_size, size_t &written) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Write_File));
	regs.rdx.r = static_cast<decltype(regs.rdx.r)>(file_handle);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(buffer);
	regs.rcx.r = buffer_size;

	const bool result = kiv_os::Sys_Call(regs);
	written = regs.rax.r;
	return result;
}

bool kiv_os_rtl::Open_File(const char* file_path, uint8_t open_mode, uint8_t file_attributes, kiv_os::THandle& file_handle) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Open_File));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_path);
	regs.rcx.r = open_mode;
	regs.rdi.r = file_attributes;

	const bool result = kiv_os::Sys_Call(regs);
	if (result) {
		file_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	}
	else {
		Last_Error = (kiv_os::NOS_Error)regs.rax.r;
	}
	return result;
}

bool kiv_os_rtl::Seek(kiv_os::THandle file_handle, kiv_os::NFile_Seek seek_operation, kiv_os::NFile_Seek new_position, size_t& position) {
	
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Seek));
	
	regs.rdx.r = static_cast<decltype(regs.rdx.r)>(file_handle);
	regs.rdi.r = static_cast<decltype(regs.rdi.r)>(position);
	regs.rcx.x = static_cast<decltype(regs.rcx.x)>(seek_operation);

	if (seek_operation == kiv_os::NFile_Seek::Set_Position) {
		regs.rcx.x = static_cast<decltype(regs.rcx.x)>(new_position);
	}

	if (seek_operation == kiv_os::NFile_Seek::Get_Position) {
		position = regs.rax.r;
	}

	bool syscall_result = kiv_os::Sys_Call(regs);

	return syscall_result;
}

bool kiv_os_rtl::Close_Handle(kiv_os::THandle handle) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Close_Handle));

	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(handle);

	bool syscall_result = kiv_os::Sys_Call(regs);
	return syscall_result;
}

bool kiv_os_rtl::Delete_File(const char* file_name) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Delete_File));
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);

	bool syscall_result = kiv_os::Sys_Call(regs);
	return syscall_result;
}

bool kiv_os_rtl::Set_Working_Dir(const char* new_dir) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Set_Working_Dir));

	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(new_dir);

	bool syscall_result = kiv_os::Sys_Call(regs);
	return syscall_result;
}

bool kiv_os_rtl::Get_Working_Dir(char* buffer, size_t buffer_size, size_t& written) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Get_Working_Dir));

	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(buffer);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(buffer_size);

	bool syscall_result = kiv_os::Sys_Call(regs);
	written = regs.rax.r;
	return syscall_result;
}

bool kiv_os_rtl::Create_Pipe(kiv_os::THandle* pipe_handles) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::File_System, static_cast<uint8_t>(kiv_os::NOS_File_System::Create_Pipe));

	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(pipe_handles);

	bool syscall_result = kiv_os::Sys_Call(regs);
	return syscall_result;
}

bool kiv_os_rtl::Create_Process(const char* file_name, const char* args, kiv_os::THandle handle_stdin, kiv_os::THandle handle_stdout, kiv_os::THandle& process) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));

	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(kiv_os::NClone::Create_Process);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(file_name);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(args);
	regs.rbx.e = (handle_stdin << 16) | handle_stdout;

	bool syscall_result = kiv_os::Sys_Call(regs);

	process = static_cast<kiv_os::THandle>(regs.rax.r);

	return syscall_result;
}

bool kiv_os_rtl::Create_Thread(kiv_os::TThread_Proc* proc, void* data, kiv_os::THandle& process) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Clone));

	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(kiv_os::NClone::Create_Thread);
	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(proc);
	regs.rdi.r = reinterpret_cast<decltype(regs.rdi.r)>(data);

	bool syscall_result = kiv_os::Sys_Call(regs);

	process = static_cast<kiv_os::THandle>(regs.rax.r);

	return syscall_result;
}

bool kiv_os_rtl::Wait_For(kiv_os::THandle* handle_field, int handle_count, kiv_os::THandle& signalized_handle) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Wait_For));

	regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(handle_field);
	regs.rcx.r = static_cast<decltype(regs.rcx.r)>(handle_count);

	bool syscall_result = kiv_os::Sys_Call(regs);
	signalized_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	return syscall_result;
}

bool kiv_os_rtl::Read_Exit_Code(kiv_os::THandle process_handle, uint16_t& exit_code) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Read_Exit_Code));

	regs.rdx.x = static_cast<decltype(regs.rdx.x)>(process_handle);

	bool syscall_result = kiv_os::Sys_Call(regs);
	exit_code = static_cast<size_t>(regs.rax.x);
	return syscall_result;
}

bool kiv_os_rtl::Exit(uint16_t exit_code) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Exit));
	
	regs.rcx.r = static_cast<decltype(regs.rdx.r)>(exit_code);

	bool syscall_result = kiv_os::Sys_Call(regs);
	return syscall_result;
}

bool kiv_os_rtl::Shutdown() {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Shutdown));
	bool syscall_result = kiv_os::Sys_Call(regs);
	return syscall_result;
}

bool kiv_os_rtl::Register_Signal_Handler(kiv_os::NSignal_Id signal_Id, kiv_os::TThread_Proc* proc) {
	kiv_hal::TRegisters regs = Prepare_SysCall_Context(kiv_os::NOS_Service_Major::Process, static_cast<uint8_t>(kiv_os::NOS_Process::Register_Signal_Handler));

	regs.rcx.r = static_cast<decltype(regs.rdx.r)>(signal_Id);

	if (proc == 0) {
		regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(Default_Signal);
	}
	else {
		regs.rdx.r = reinterpret_cast<decltype(regs.rdx.r)>(proc);
	}

	bool syscall_result = kiv_os::Sys_Call(regs);
	return syscall_result;
}
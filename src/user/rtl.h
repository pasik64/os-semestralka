#pragma once

#include "..\api\api.h"
#include <atomic>
#include <string>

namespace kiv_os_rtl {

	extern std::atomic<kiv_os::NOS_Error> Last_Error;
    const int buffer_size = 256;
	const int fat_buffer_size = 4096;

	void Default_Signal();

	bool Read_File(const kiv_os::THandle file_handle, char* const buffer, const size_t buffer_size, size_t &read);
		//zapise do souboru identifikovaneho deskriptor data z buffer o velikosti buffer_size a vrati pocet zapsanych dat ve written
		//vraci true, kdyz vse OK

	bool Write_File(const kiv_os::THandle file_handle, const char *buffer, const size_t buffer_size, size_t &written);
	//zapise do souboru identifikovaneho deskriptor data z buffer o velikosti buffer_size a vrati pocet zapsanych dat ve written
	//vraci true, kdyz vse OK

	bool Open_File(const char* file_path, uint8_t open_mode, uint8_t file_attributes, kiv_os::THandle &file_handle);

	bool Seek(kiv_os::THandle file_handle, kiv_os::NFile_Seek seek_operation, kiv_os::NFile_Seek new_position, size_t& position);
	bool Close_Handle(kiv_os::THandle handle);
	bool Delete_File(const char* file_name);
	bool Set_Working_Dir(const char* new_dir);
	bool Get_Working_Dir(char* buffer, size_t buffer_size, size_t& written);
	bool Create_Pipe(kiv_os::THandle* pipe_handles);


	bool Create_Process(const char* file_name, const char* args, kiv_os::THandle handle_stdin, kiv_os::THandle handle_stdout, kiv_os::THandle& process);
	bool Create_Thread(kiv_os::TThread_Proc* proc, void* data, kiv_os::THandle& process);
	bool Wait_For(kiv_os::THandle* handle_field, int handle_count, kiv_os::THandle& signalized_handle);
	bool Read_Exit_Code(kiv_os::THandle process_handle, uint16_t& exit_code);
	bool Exit(uint16_t exit_code);
	bool Shutdown();

	bool Register_Signal_Handler(kiv_os::NSignal_Id signal_Id, kiv_os::TThread_Proc* proc);

}
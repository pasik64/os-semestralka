
#include "shell.h"
#include "errors.h"
#include "rtl.h"
#include "command.h"
#include "command_exec.h"


bool echo_on = true;
bool run = true;
bool kill = false;

void kill_all() {
	run = false;
	kill = true;
	kiv_os_rtl::Shutdown();
}

size_t __stdcall shell(const kiv_hal::TRegisters &regs) {

	const kiv_os::THandle std_in = static_cast<kiv_os::THandle>(regs.rax.x);
	const kiv_os::THandle std_out = static_cast<kiv_os::THandle>(regs.rbx.x);

	const size_t buffer_size = 256;
	char buffer[buffer_size];
	size_t counter_in;
	size_t counter_out;
	std::vector<kiv_os_command::command> commands;
	std::vector<std::string> temp_test_parse;
	const char* new_line = "\n";
	const int path_length = 256;
	char path[path_length];
	const char* intro = "Shell \n";
	kiv_os_rtl::Write_File(std_out, intro, strlen(intro), counter_out);

	bool prompt_visible_parent = echo_on;
	echo_on = true;
	
	do {
		memset(buffer, 0, buffer_size);


		memset(path, 0, path_length);
		kiv_os_rtl::Get_Working_Dir(path, path_length, counter_in);
		strcat_s(path, ">");
		if (!run) {
			return 0;
		}
		kiv_os_rtl::Write_File(std_out, path, strlen(path), counter_out);

		if (kiv_os_rtl::Read_File(std_in, buffer, buffer_size, counter_in)) {
			if ((counter_in > 0) && (counter_in == buffer_size)) counter_in--;
			buffer[counter_in] = 0;

			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter_out);

			if (echo_on) {
				kiv_os_rtl::Write_File(std_out, buffer, counter_in, counter_out);
				kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter_out);
			}

			commands = kiv_os_command::parse_commands(buffer, counter_in);

			if (commands.empty()) {
				continue;
			}

			if (!kiv_os_command_exec::exec_commands(&commands, std_in, std_out)) {
				run = false;
			}

			kiv_os_rtl::Write_File(std_out, new_line, strlen(new_line), counter_out);

		}
		else {
			kiv_os_error::process_error(kiv_os_rtl::Last_Error, std_out);
		}

	} while (run);

	if (!kill) {
		run = true;
	}
	echo_on = prompt_visible_parent;

	return 0;
}
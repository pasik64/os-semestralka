#pragma once

#include "rtl.h"
#include "command_parser.h"
#include "..\api\api.h"

extern bool echo_on;

extern "C" size_t __stdcall shell(const kiv_hal::TRegisters &regs);

void kill_all();
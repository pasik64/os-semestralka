#include "shutdown.h"

size_t __stdcall shutdown(const kiv_hal::TRegisters &regs) {
	kill_all();
	return (size_t)kiv_os::NOS_Error::Success;
}
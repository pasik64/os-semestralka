#include "sort.h"

bool comparator_normal(std::string string_1, std::string string_2) {
	return string_1 < string_2;
}

size_t __stdcall sort(const kiv_hal::TRegisters& regs) {
	kiv_os::THandle in = static_cast<kiv_os::THandle>(regs.rax.x);
	kiv_os::THandle out = static_cast<kiv_os::THandle>(regs.rbx.x);

	char buffer[kiv_os_rtl::fat_buffer_size];
	size_t counter;
	std::stringstream stream;
	std::string token;
	std::vector<std::string> tokens;

	while (kiv_os_rtl::Read_File(in, buffer, kiv_os_rtl::fat_buffer_size, counter)) {
		if (counter == 1 && buffer[0] == 0) {
			break;
		}
		stream << buffer;
	}

	while (std::getline(stream, token, '\n')) {
		tokens.push_back(token);
	}

	std::sort(tokens.begin(), tokens.end(), comparator_normal);

	for (std::string& token2 : tokens) {
		token2.append("\n");
		if (!kiv_os_rtl::Write_File(out, token2.c_str(), token2.length(), counter)) {
			return (size_t)kiv_os::NOS_Error::IO_Error;
		}
	}

	return (size_t)kiv_os::NOS_Error::Success;
}
#pragma once

#include "..\api\api.h"
#include "rtl.h"

#include <sstream>
#include <vector>
#include <algorithm>

bool comparator_normal(std::string string_1, std::string string_2);

extern "C" size_t __stdcall sort(const kiv_hal::TRegisters & regs);
#include "tasklist.h"

size_t __stdcall tasklist(const kiv_hal::TRegisters& regs) {
	kiv_os::THandle in = static_cast<kiv_os::THandle>(regs.rax.x);
	kiv_os::THandle out = static_cast<kiv_os::THandle>(regs.rbx.x);

	char buffer[kiv_os_rtl::fat_buffer_size];
	size_t counter_read;
	size_t counter_write;

	if (!kiv_os_rtl::Open_File("PROC_ADDRESS", 0, (uint8_t)kiv_os::NFile_Attributes::Read_Only, in)) {
		return (size_t) kiv_os::NOS_Error::Permission_Denied;
	}

	if (!kiv_os_rtl::Read_File(in, buffer, kiv_os_rtl::fat_buffer_size, counter_read)) {
		return (size_t) kiv_os::NOS_Error::IO_Error;
	}

	if (!kiv_os_rtl::Write_File(out, buffer, counter_read, counter_write)) {
		return (size_t)kiv_os::NOS_Error::IO_Error;
	}

	return (size_t)kiv_os::NOS_Error::Success;
}
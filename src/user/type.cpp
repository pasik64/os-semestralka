#include "type.h"

size_t __stdcall type(const kiv_hal::TRegisters& regs) {
	kiv_os::THandle in_handle = static_cast<kiv_os::THandle>(regs.rax.x);
	kiv_os::THandle out_handle = static_cast<kiv_os::THandle>(regs.rbx.x);

	char* tokens = reinterpret_cast<char*>(regs.rdi.r);
	std::stringstream stream(tokens);
	std::string token;
	std::vector<std::string> parameters;
	size_t counter;

	while (std::getline(stream, token, ' ')) {
		parameters.push_back(token);
	}

	if (!parameters.size()) {
		kiv_os_rtl::Write_File(out_handle, "Command was called with invalid arguments\n", sizeof("Command was called with invalid arguments\n"), counter);
		return (size_t)kiv_os::NOS_Error::Invalid_Argument;
	} else {
		kiv_os::THandle handler;
		for (int i = 0; i < parameters.size(); i++) {
			if (!kiv_os_rtl::Open_File(parameters[i].c_str(), (uint8_t)kiv_os::NOpen_File::fmOpen_Always, (uint8_t)kiv_os::NFile_Attributes::Read_Only, handler)) {
				return (size_t)kiv_os::NOS_Error::Permission_Denied;
			}

			size_t position = 0;
			if (!kiv_os_rtl::Seek(handler, kiv_os::NFile_Seek::Set_Position, kiv_os::NFile_Seek::Beginning, position)) {
				return (size_t)kiv_os::NOS_Error::IO_Error;
			}

			size_t counter_in, counter_out;
			char buffer[kiv_os_rtl::buffer_size];
			while (kiv_os_rtl::Read_File(handler, buffer, kiv_os_rtl::buffer_size, counter_in)) {
				if (counter_in == 0) {
					break;
				}

				if (!kiv_os_rtl::Write_File(out_handle, buffer, counter_in, counter_out)) {
					return (size_t)kiv_os::NOS_Error::IO_Error;
				}
			}

			kiv_os_rtl::Close_Handle(in_handle);
			return (size_t)kiv_os::NOS_Error::Success;
		}
	}
	return (size_t)kiv_os::NOS_Error::Invalid_Argument;
	
}
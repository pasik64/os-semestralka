#pragma once

#include "..\api\api.h"
#include "rtl.h"

#include <sstream>
#include <vector>
#include <algorithm>

#define BUFFER_SIZE 256

extern "C" size_t __stdcall type(const kiv_hal::TRegisters & regs);
